import * as React from 'react';
import styled, { css } from 'styled-components';

import { color } from '../theme';


const Progress = styled.div`
  height: 100%;
  background-color: ${color.primary};
  
  ${props => css`
    
    ${props.width && css`
      width: ${props.width}%;
    `}
  `}
`;

const Wrapper = styled.div`
  height: 5px;
  background-color: #bebfc3;
  border-radius: 15px;
  overflow: hidden;
  
  ${props => css`

    ${props.isDark && css`
      background-color: #3d4348;
    `}
  `}
`;


export const ProgressBar = (props) => {

  return (
    <Wrapper className={ props.className } isDark={ props.isDark }>
      <Progress width={ props.width }/>
    </Wrapper>
  );
};
