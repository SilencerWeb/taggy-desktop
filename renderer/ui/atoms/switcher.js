import * as React from 'react';
import styled, { css } from 'styled-components';

import { color } from '../theme';


export const Switcher = styled.div`
  position: relative;
  width: 45px;
  height: 27px;
  border-radius: 15px;
  box-shadow: inset 0 0 0 1px #d5d5d5;
  cursor: pointer;
  
  &:before {
    content: "";
    position: absolute;
    top: 0;
    left: 0;
    width: 27px;
    height: 27px;
    border-radius: 15px;
    background: rgba(19, 191, 17, 0);
    transition: 0.25s ease-in-out;
  }
  
  &:after {
    content: "";
    position: absolute;
    top: 0;
    left: 0;
    width: 27px;
    height: 27px;
    background: #ffffff;
    border-radius: 15px;
    box-shadow: inset 0 0 0 1px rgba(0, 0, 0, 0.2), 0 2px 4px rgba(0, 0, 0, 0.2);
    transition: 0.25s ease-in-out;
  }
  
  ${props => css`
    
    ${props.active && css`
      
      &:before {
        width: 47px;
        background: #1e8aff;
      }
      
      &:after {
        left: 20px;
        box-shadow: inset 0 0 0 1px ${color.primary}, 0 2px 4px rgba(0, 0, 0, 0.2);
      }  
    `}
  `}
`;
