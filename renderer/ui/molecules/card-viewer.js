import * as React from 'react';
import styled, { css } from 'styled-components';
import ReactTooltip from 'react-tooltip';
import { withFormik } from 'formik';
import * as Yup from 'yup';
import { ipcRenderer } from 'electron';
import CreatableSelect from 'react-select/lib/Creatable';

import { CloseIcon, FilledStarIcon, StarIcon, CopyIcon, PencilIcon, BucketIcon } from '../outlines';

import { color } from '../theme';


const Button = styled.button`
  background: none;
  border: 1px solid #f5f5f5;
  border-radius: 4px;
  padding-top: 10px;
  padding-right: 20px;
  padding-bottom: 10px;
  padding-left: 20px;
  outline: none;
  cursor: pointer;
  transition: 0.1s;
`;

const CancelButton = styled(Button)`
  margin-right: 20px;

  &:hover {
    border-color: ${color.error} !important;
  }
`;

const SaveButton = styled(Button)`

  &:hover {
    border-color: ${color.primary} !important;
  }
`;

const Label = styled.label`
  display: inline-block;
  vertical-align: top;
  font-size: 14px;
  margin-bottom: 5px;
`;

const Input = styled.input`
  width: 100%;
  min-height: 36px;
  font-family: "SF Pro Display", sans-serif;
  font-size: 12px;
  line-height: 1;
  color: ${color.text.primary};
  border: 1px solid #f5f5f5;
  border-radius: 4px;
  padding-top: 10px;
  padding-right: 10px;
  padding-bottom: 10px;
  padding-left: 10px;
  margin-bottom: 20px;
  outline: none;
  transition: 0.1s;
  
  &:last-child {
    margin-bottom: 0;
  }
  
  &:focus {
    border-color: ${color.primary};
  }
  
  ${props => css`
    
    ${props.error && css`
      border-color: ${color.error};
      
      &:focus {
        border-color: ${color.error};
      }
    `}
  `}
`;

const Textarea = styled.textarea`
  flex-grow: 1;
  width: 100%;
  min-height: 36px;
  font-family: "SF Pro Display", sans-serif;
  font-size: 12px;
  line-height: 1.45;
  color: ${color.text.primary};
  border: 1px solid #f5f5f5;
  border-radius: 4px;
  padding-top: 10px;
  padding-right: 10px;
  padding-bottom: 10px;
  padding-left: 10px;
  margin-bottom: 20px;
  outline: none;
  resize: none;
  transition: 0.1s;
  
  &:last-child {
    margin-bottom: 0;
  }
  
  &:focus {
    border-color: ${color.primary};
  }
  
  ${props => css`
    
    ${props.error && css`
      border-color: ${color.error};
      
      &:focus {
        border-color: ${color.error};
      }
    `}
  `}
`;

const TagsFieldWrapper = styled.div`
  margin-bottom: 10px;
  
  &:last-child {
    margin-bottom: 0;
  }
`;

const Form = styled.form`
  flex-grow: 1;
  flex-shrink: 0;
  display: flex;
  flex-direction: column;
`;

const Title = styled.div`
  flex-shrink: 0;
  font-size: 20px;
  line-height: 1.3;
  font-weight: 600;
  word-wrap: break-word;
  margin-bottom: 20px;
`;

const TextLine = styled.div`
  min-height: 20px;
  word-wrap: break-word;
`;

const Text = styled.div`
  flex-shrink: 0;
  font-size: 14px;
  margin-bottom: 20px;
`;

const Image = styled.img`
  max-width: 100%;
`;

const ImageWrapper = styled.div`
  flex-grow: 1;
  flex-shrink: 0;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-bottom: 20px;
`;

const CloseButton = styled.div`
  cursor: pointer;
  
  svg {
    transition: 0.1s;
  
    &:hover {
      fill: ${color.primary};
    }
  }
`;

const FavoriteButton = styled.div`
  margin-right: 20px;
  cursor: pointer;
  
  &:hover {
  
    svg {
      fill: #ffbd2e;
    }
  }
  
  svg {
    transition: 0.1s;
  }
`;

const CopyButton = styled.div`
  margin-right: 20px;
  cursor: pointer;
  
  &:hover {
  
    svg {
      fill: ${color.primary};
    }
  }
  
  svg {
    transition: 0.1s;
  }
`;

const EditButton = styled.div`
  margin-right: 20px;
  cursor: pointer;
  
  svg {
    transition: 0.1s;
  
    &:hover {
      fill: ${color.primary};
    }
  }
`;

const DeleteButton = styled.div`
  cursor: pointer;
  
  svg {
    transition: 0.1s;
  
    &:hover {
      fill: ${color.error};
    }
  }
`;

const ActionsSide = styled.div`
  display: flex;
`;

const Actions = styled.div`
  flex-shrink: 0;
  display: flex;
  justify-content: space-between;
  margin-bottom: 20px;
`;

const Tag = styled.div`
  font-size: 14px;
  margin-right: 7.5px;
  transition: 0.1s;
  cursor: pointer;
  
  &:last-child {
    margin-right: 0;
  }
  
  &:hover {
    color: ${color.primary};
  }
`;

const Tags = styled.div`
  flex-shrink: 0;
  display: flex;
  flex-wrap: wrap;
  margin-top: auto;
`;

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: 75%;
  max-height: calc(100vh - 60px);
  border-left: 1px solid #f5f5f5;
  padding-top: 20px;
  padding-right: 20px;
  padding-bottom: 20px;
  padding-left: 20px;
  overflow-y: auto;
  overflow-x: hidden;
  
  ${props => css`
  
    ${props.isDark && css`
      color: #acacac;
      border-left-color: #3d4348;
      
      ${Button} {
        color: #acacac;
        border-color: #3d4348;
      }
      
      ${Input}, ${Textarea} {
        color: #acacac;
        background-color: #3d4348;
        border-color: transparent;
        
        &:focus {
          border-color: ${color.primary};
        }
      }
    `}
  `}
`;


class CardViewerComponent extends React.Component {

  handleFavoriteButtonClick = () => {
    const card = {
      id: this.props.card.id,
      isSorted: true,
      isFavorite: !this.props.card.isFavorite,
      updatedAt: new Date(),
    };

    ipcRenderer.send('edit-card', card);
  };

  handleCopyButtonClick = () => {
    ipcRenderer.send('copy', this.props.card.id);
  };

  handleEditButtonClick = () => {
    this.props.onEditButtonClick(this.props.card);
  };

  handleDeleteButtonClick = () => {
    ipcRenderer.send('delete-card', this.props.card.id);
  };

  handleCancelButtonClick = () => {
    this.props.onCancelButtonClick(this.props.card.type === 'link');
  };

  render = () => {
    const { type, title, text, link, imagePath, tags, isFavorite } = this.props.card;

    const selectValueOptions = this.props.values.tags && this.props.values.tags.map((tag) => ({
      label: tag.label[0] === '#' ? tag.label.slice(1) : tag.label,
      value: tag.value,
    }));

    const selectOptions = this.props.tags && this.props.tags.map((tag) => ({
      label: tag.name[0] === '#' ? tag.name.slice(1) : tag.name,
      value: tag.name,
    }));

    return (
      <React.Fragment>
        {
          this.props.isEditing ?
            <Wrapper className={ this.props.className } isDark={ this.props.isDark }>
              <Actions>
                <ActionsSide>
                  <CloseButton onClick={ this.props.onCloseButtonClick }>
                    <CloseIcon width={ 22 } height={ 22 } fill={ this.props.isDark ? '#acacac' : color.text.primary }/>
                  </CloseButton>
                </ActionsSide>

                <ActionsSide>
                  <CancelButton onClick={ this.handleCancelButtonClick }>Cancel</CancelButton>
                  <SaveButton onClick={ this.props.submitForm }>Save</SaveButton>
                </ActionsSide>
              </Actions>

              <Form onSubmit={ this.props.handleSubmit }>
                {
                  type !== 'image' &&
                  <React.Fragment>
                    <Label htmlFor={ 'title' }>Title</Label>
                    <Input
                      id={ 'title' }
                      name={ 'title' }
                      value={ this.props.values.title || '' }
                      error={ !!this.props.errors.title }
                      onChange={ this.props.handleChange }
                      onBlur={ this.props.handleBlur }
                    />
                  </React.Fragment>
                }

                {
                  type !== 'image' &&
                  <React.Fragment>
                    <Label htmlFor={ 'text' }>Note or description { type === 'text' && '*' }</Label>
                    <Textarea
                      id={ 'text' }
                      name={ 'text' }
                      value={ this.props.values.text || '' }
                      error={ !!this.props.errors.text }
                      onChange={ this.props.handleChange }
                      onBlur={ this.props.handleBlur }
                    />
                  </React.Fragment>
                }

                {
                  type === 'link' && link &&
                  <React.Fragment>
                    <Label htmlFor={ 'link' }>Link *</Label>
                    <Input
                      id={ 'link' }
                      name={ 'link' }
                      value={ this.props.values.link || '' }
                      error={ !!this.props.errors.link }
                      onChange={ this.props.handleChange }
                      onBlur={ this.props.handleBlur }
                    />
                  </React.Fragment>
                }

                <TagsFieldWrapper>
                  <Label htmlFor={ 'tags' }>Tags</Label>

                  <CreatableSelect
                    className={ this.props.isDark && 'react-select--dark' }
                    classNamePrefix={ 'react-select' }
                    inputId={ 'tags' }
                    name={ 'tags' }
                    placeholder={ '' }
                    value={ selectValueOptions }
                    options={ selectOptions }
                    menuPlacement={ 'top' }
                    isMulti={ true }
                    isValidNewOption={ (inputValue, selectValue, selectOptions) => inputValue && !inputValue.includes(' ') && selectOptions.every((selectOption) => selectOption.label !== inputValue) }
                    onChange={ (value) => this.props.setFieldValue('tags', value) }
                    onBlur={ () => this.props.setFieldTouched('tags', true) }
                    components={ {
                      ClearIndicator: (props) => {
                        const { getStyles, innerProps: { ref, ...restInnerProps } } = props;
                        return (
                          <div className={ 'react-select__clear-indicator' } { ...restInnerProps } ref={ ref } style={ getStyles('clearIndicator', props) }>
                            <div style={ { padding: '0px 5px' } }>
                              <CloseIcon width={ 12.5 } height={ 12.5 } fill={ this.props.isDark ? '#acacac' : color.text.primary }/>
                            </div>
                          </div>
                        );
                      },
                    } }
                  />
                </TagsFieldWrapper>
              </Form>
            </Wrapper>
            :
            <Wrapper className={ this.props.className } isDark={ this.props.isDark }>
              <Actions>
                <ActionsSide>
                  <CloseButton
                    data-tip
                    data-for={ 'cardViewerCloseButton' }
                    data-effect={ 'solid' }
                    onClick={ this.props.onCloseButtonClick }
                  >
                    <CloseIcon width={ 22 } height={ 22 } fill={ this.props.isDark ? '#acacac' : color.text.primary }/>
                  </CloseButton>

                  <ReactTooltip id={ 'cardViewerCloseButton' } className={ this.props.isDark ? 'react-tooltip react-tooltip_dark' : 'react-tooltip' } delayShow={ 750 }>
                    <span>Close</span>
                  </ReactTooltip>
                </ActionsSide>

                <ActionsSide>
                  <FavoriteButton
                    data-tip
                    data-for={ 'cardViewerFavoriteButton' }
                    data-effect={ 'solid' }
                    onClick={ this.handleFavoriteButtonClick }
                  >
                    {
                      isFavorite ?
                        <FilledStarIcon width={ 22 } height={ 22 } fill={ '#ffbd2e' }/>
                        :
                        <StarIcon width={ 22 } height={ 22 } fill={ this.props.isDark ? '#acacac' : color.text.primary }/>
                    }
                  </FavoriteButton>

                  <ReactTooltip id={ 'cardViewerFavoriteButton' } className={ this.props.isDark ? 'react-tooltip react-tooltip_dark' : 'react-tooltip' } delayShow={ 750 }>
                    <span>{ isFavorite ? 'Remove from favorite' : 'Add to favorite' }</span>
                  </ReactTooltip>

                  <CopyButton
                    data-tip
                    data-for={ 'cardViewerCopyButton' }
                    data-effect={ 'solid' }
                    onClick={ this.handleCopyButtonClick }
                  >
                    <CopyIcon width={ 22 } height={ 22 } fill={ this.props.isDark ? '#acacac' : color.text.primary }/>
                  </CopyButton>

                  <ReactTooltip id={ 'cardViewerCopyButton' } className={ this.props.isDark ? 'react-tooltip react-tooltip_dark' : 'react-tooltip' } delayShow={ 750 }>
                    <span>Copy</span>
                  </ReactTooltip>

                  <EditButton
                    data-tip
                    data-for={ 'cardViewerEditButton' }
                    data-effect={ 'solid' }
                    onClick={ this.handleEditButtonClick }
                  >
                    <PencilIcon width={ 22 } height={ 22 } fill={ this.props.isDark ? '#acacac' : color.text.primary }/>
                  </EditButton>

                  <ReactTooltip id={ 'cardViewerEditButton' } className={ this.props.isDark ? 'react-tooltip react-tooltip_dark' : 'react-tooltip' } delayShow={ 750 }>
                    <span>Edit</span>
                  </ReactTooltip>

                  <DeleteButton
                    data-tip
                    data-for={ 'cardViewerDeleteButton' }
                    data-effect={ 'solid' }
                    onClick={ this.handleDeleteButtonClick }
                  >
                    <BucketIcon width={ 22 } height={ 22 } fill={ this.props.isDark ? '#acacac' : color.text.primary }/>
                  </DeleteButton>

                  <ReactTooltip id={ 'cardViewerDeleteButton' } className={ this.props.isDark ? 'react-tooltip react-tooltip_dark' : 'react-tooltip' } delayShow={ 750 }>
                    <span>Delete</span>
                  </ReactTooltip>
                </ActionsSide>
              </Actions>

              { title && type !== 'image' && <Title>{ title }</Title> }
              {
                text && type !== 'image' &&
                <Text>
                  {
                    text.split('\n').map((line, i) => (
                      <TextLine key={ i }>{ line }</TextLine>
                    ))
                  }
                </Text>
              }

              {
                imagePath && type === 'image' &&
                <ImageWrapper>
                  <Image src={ `file:/${imagePath}` }/>
                </ImageWrapper>
              }

              <Tags>
                {
                  tags && tags.map((tag) => (
                    <Tag key={ tag } onClick={ () => this.props.onTagClick(tag) }>{ tag }</Tag>
                  ))
                }
              </Tags>
            </Wrapper>
        }
      </React.Fragment>
    );
  };
}

const CardViewerWithFormik = withFormik({
  enableReinitialize: true,
  mapPropsToValues: (props) => ({
    title: props.card.title,
    text: props.card.text,
    link: props.card.link,
    tags: props.card.tags.map((tag) => ({
      label: tag,
      value: tag,
    })),
  }),
  validationSchema: (props) => Yup.object().shape({
    text: props.card.type === 'text' ? Yup.string().required() : Yup.string(),
    link: props.card.type === 'link' ? Yup.string().trim().required().url() : Yup.string(),
  }),
  handleSubmit: (values, { props }) => {
    const card = {
      id: props.card.id,
      tags: values.tags.map((tag) => tag.value[0] !== '#' ? `#${tag.value.trim()}` : tag.value.trim()),
      isSorted: true,
      updatedAt: new Date(),
    };

    if (props.card.type === 'link') {
      card.title = values.title.trim();
      card.text = values.text.trim();
      card.link = values.link.trim();
    } else if (props.card.type !== 'image') {
      card.title = values.title.trim();
      card.text = values.text.trim();
    }

    ipcRenderer.send('edit-card', card);

    props.onSaveButtonClick(props.card.type === 'link');
  },
})(CardViewerComponent);

export const CardViewer = (props) => <CardViewerWithFormik { ...props }/>; 