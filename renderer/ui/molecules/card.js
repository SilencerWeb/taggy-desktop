import * as React from 'react';
import styled, { css } from 'styled-components';
import ReactTooltip from 'react-tooltip';
import { ipcRenderer } from 'electron';

import {
  LinkIcon,
  TextIcon,
  PictureIcon,
  FilledStarIcon,
  StarIcon,
  CopyIcon,
  PencilIcon,
  BucketIcon,
  EyeIcon,
} from '../outlines';

import { color } from '../theme';

import { getHostnameFromUrl, rgba } from '../../utils';


const Image = styled.img`
  position: absolute;
  top: 50%;
  left: 50%;
  width: calc(100% + 1px);
  height: calc(100% + 1px);
  object-fit: cover;
  transform: translate(-50%, -50%);
`;

const Title = styled.div`
  flex-shrink: 0;
  max-height: 44px;
  font-size: 17px;
  line-height: 1.3;
  font-weight: 600;
  text-overflow: ellipsis;
  word-wrap: break-word;
  margin-bottom: 5px;
  overflow: hidden;
`;

const TextLine = styled.div`
  min-height: 18px;
  word-wrap: break-word;
`;

const Text = styled.div`
  margin-bottom: 5px;
  overflow: hidden;
  
  &:last-child {
    margin-bottom: 0;
  }
`;

const Link = styled.div`
  margin-top: auto;
`;

const FavoriteButton = styled.div`
  margin-right: 10px;
  cursor: pointer;
  
  &:hover {
  
    svg {
      fill: #ffbd2e;
    }
  }
  
  svg {
    transition: 0.1s;
  }
`;

const CopyButton = styled.div`
  margin-right: 10px;
  cursor: pointer;
  
  &:hover {
  
    svg {
      fill: ${color.text.primary};
    }
  }
  
  svg {
    transition: 0.1s;
  }
`;

const EditButton = styled.div`
  margin-right: 10px;
  cursor: pointer;
  
  &:hover {
  
    svg {
      fill: ${color.text.primary};
    }
  }
  
  svg {
    transition: 0.1s;
  }
`;

const DeleteButton = styled.div`
  cursor: pointer;
  
  &:hover {
  
    svg {
      fill: ${color.error};
    }
  }
  
  svg {
    transition: 0.1s;
  }
`;

const ActionsSide = styled.div`
  display: flex;
`;

const Actions = styled.div`
  display: flex;
  justify-content: space-between;
  width: 100%;
`;

const OpenButton = styled.div`
  flex-grow: 1;
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;
  
  &:hover {
  
    svg {
      fill: ${color.text.primary};
    }
  }
  
  svg {
    transition: 0.1s;
  }
`;

const Tag = styled.div`
  font-size: 13px;
  margin-right: 5px;
  cursor: pointer;
  transition: 0.1s;

  &:last-child {
    margin-right: 0;
  }
  
  &:hover {
    color: ${color.text.primary};
  }
`;

const Tags = styled.div`
  display: flex;
  max-height: 36px;
  flex-wrap: wrap;
  margin-top: auto;
  overflow: hidden;
`;

const Overlay = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  z-index: 9;
  display: flex;
  flex-direction: column;
  width: calc(100% + 1px); // because of the Wrapper's border
  height: calc(100% + 1px); // because of the Wrapper's border
  color: ${color.text.secondary};
  background-color: ${rgba(color.primary, 0.85)};
  padding-top: 8.5px;
  padding-right: 8.5px;
  padding-bottom: 8.5px;
  padding-left: 8.5px;
  opacity: 0;
  visibility: hidden;
  transition: 0.1s;
`;

const TransparentOverlay = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: calc(100% + 1px); // because of the Wrapper's border
  height: calc(100% + 1px); // because of the Wrapper's border
  background-color: ${rgba(color.primary, 0.85)};
`;

const Wrapper = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  min-width: 240px;
  height: 240px;
  border-right: 1px solid #f5f5f5;
  border-bottom: 1px solid #f5f5f5;
  padding-top: 8.5px;
  padding-right: 8.5px;
  padding-bottom: 8.5px;
  padding-left: 8.5px;
  
  &:hover {
  
    ${Overlay} {
      opacity: 1;
      visibility: visible;
    }
  }
  
  ${props => css`

    ${props.isDark && css`
      color: #acacac;
      border-right-color: #3d4348;
      border-bottom-color: #3d4348;
      
      ${OpenButton} {
      
        &:hover {
        
          svg {
            fill: #2d3235 !important;
          }
        }
      }
    `}
    
    ${props.size === 'normal' && css`
      width: 25%;
    `}
    
    ${props.size === 'large' && css`
      width: 50%;
    `}
    
    ${props.isActive && css`
      color: ${color.text.secondary};
      background-color: ${color.primary};
      border: none;
      
      &:hover {
    
        ${Overlay} {
          opacity: 0;
          visibility: hidden;
        }
      }
    `}
    
    ${props.isCardViewerActive && css`
      width: 100%;
      border-right-color: transparent;
      
      &:last-child {
        border-bottom-color: transparent;
      }
      
      &:nth-of-type(2) {
        border-bottom-color: ${props.isDark ? '#3d4348' : '#f5f5f5'};
      }
    `}
  `}
`;


export const Card = (props) => {
  const { id, type, title, text, link, imagePath, tags, isFavorite } = props.card;

  const handleFavoriteButtonClick = () => {
    const card = {
      id: props.card.id,
      isSorted: true,
      isFavorite: !props.card.isFavorite,
      updatedAt: new Date(),
    };

    ipcRenderer.send('edit-card', card);
  };

  const handleCopyButtonClick = () => {
    ipcRenderer.send('copy', id);
  };

  const handleEditButtonClick = () => {
    props.onEditButtonClick(props.card);
  };

  const handleDeleteButtonClick = () => {
    ipcRenderer.send('delete-card', props.card.id);
  };

  const handleOpenButtonClick = () => {
    props.onOpenButtonClick(props.card);
  };

  return (
    <Wrapper
      className={ props.className }
      size={ 'normal' } // should be text && text.length > 800 ? 'large' : 'normal' later
      isDark={ props.isDark }
      isActive={ props.isActive }
      isCardViewerActive={ props.isCardViewerActive }
    >
      { imagePath && type === 'image' && <Image src={ `file:/${imagePath}` }/> }

      <Overlay>
        <Actions>
          <ActionsSide>
            { type && type === 'link' && <LinkIcon width={ 18 } height={ 18 } fill={ color.secondary }/> }
            { type && type === 'text' && <TextIcon width={ 18 } height={ 18 } fill={ color.secondary }/> }
            { type && type === 'image' && <PictureIcon width={ 18 } height={ 18 } fill={ color.secondary }/> }
          </ActionsSide>

          <ActionsSide>
            <FavoriteButton
              data-tip
              data-for={ 'cardFavoriteButton' }
              data-effect={ 'solid' }
              onClick={ handleFavoriteButtonClick }
            >
              {
                isFavorite ?
                  <FilledStarIcon width={ 18 } height={ 18 } fill={ '#ffbd2e' }/>
                  :
                  <StarIcon width={ 18 } height={ 18 } fill={ color.secondary }/>
              }
            </FavoriteButton>

            <ReactTooltip id={ 'cardFavoriteButton' } className={ props.isDark ? 'react-tooltip react-tooltip_dark' : 'react-tooltip' } delayShow={ 750 }>
              <span>{ isFavorite ? 'Remove from favorite' : 'Add to favorite' }</span>
            </ReactTooltip>

            <CopyButton
              data-tip
              data-for={ 'cardCopyButton' }
              data-effect={ 'solid' }
              onClick={ handleCopyButtonClick }
            >
              <CopyIcon width={ 18 } height={ 18 } fill={ color.secondary }/>
            </CopyButton>

            <ReactTooltip id={ 'cardCopyButton' } className={ props.isDark ? 'react-tooltip react-tooltip_dark' : 'react-tooltip' } delayShow={ 750 }>
              <span>Copy</span>
            </ReactTooltip>

            <EditButton
              data-tip
              data-for={ 'cardEditButton' }
              data-effect={ 'solid' }
              onClick={ handleEditButtonClick }
            >
              <PencilIcon width={ 18 } height={ 18 } fill={ color.secondary }/>
            </EditButton>

            <ReactTooltip id={ 'cardEditButton' } className={ props.isDark ? 'react-tooltip react-tooltip_dark' : 'react-tooltip' } delayShow={ 750 }>
              <span>Edit</span>
            </ReactTooltip>

            <DeleteButton
              data-tip
              data-for={ 'cardDeleteButton' }
              data-effect={ 'solid' }
              onClick={ handleDeleteButtonClick }
            >
              <BucketIcon width={ 18 } height={ 18 } fill={ color.secondary }/>
            </DeleteButton>

            <ReactTooltip id={ 'cardDeleteButton' } className={ props.isDark ? 'react-tooltip react-tooltip_dark' : 'react-tooltip' } delayShow={ 750 }>
              <span>Delete</span>
            </ReactTooltip>
          </ActionsSide>
        </Actions>

        <OpenButton
          data-tip
          data-for={ 'cardOpenButton' }
          data-effect={ 'float' }
          onClick={ handleOpenButtonClick }
        >
          <EyeIcon width={ 50 } height={ 50 } fill={ color.secondary }/>
        </OpenButton>

        <ReactTooltip id={ 'cardOpenButton' } className={ props.isDark ? 'react-tooltip react-tooltip_dark' : 'react-tooltip' }>
          <span>{ type && type === 'link' ? 'Open in the browser' : 'Open' }</span>
        </ReactTooltip>

        <Tags>
          {
            tags && tags.map((tag, i) => (
              <Tag key={ i } onClick={ () => props.onTagClick(tag) }>{ tag }</Tag>
            ))
          }
        </Tags>
      </Overlay>

      { props.isActive && type && type === 'image' && <TransparentOverlay/> }

      { title && type !== 'image' && <Title>{ title.slice(0, 50) }</Title> }
      {
        text && type !== 'image' &&
        <Text>
          {
            text.slice(0, 550).split('\n').map((line, i) => (
              <TextLine key={ i }>{ line }</TextLine>
            ))
          }
        </Text>
      }
      { link && type !== 'image' && <Link>{ getHostnameFromUrl(link) }</Link> }
    </Wrapper>
  );
};
