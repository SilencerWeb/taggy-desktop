import * as React from 'react';
import styled, { css } from 'styled-components';
import { ipcRenderer } from 'electron';

import { color } from '../theme';

import { CloseIcon, CogIcon } from '../outlines';


const Input = styled.input`
  width: 100%;
  font-family: "Open Sans", sans-serif;
  font-size: 12px;
  line-height: 1;
  color: ${color.text.primary};
  border: 1px solid #f5f5f5;
  border-radius: 100px;
  padding-top: 10px;
  padding-right: 40px;
  padding-bottom: 10px;
  padding-left: 15px;
  outline: none;
  transition: 0.1s;
        
  &::placeholder {
    color: ${color.text.primary};
  }
  
  &:focus {
    border-color: ${color.primary};
  }
  
  ${props => css`
    
    ${props.isFilled && css`
      border-color: ${props.isDark ? '#9f9f9f !important' : '#bebfc3 !important'};
      
      &:focus {
        border-color: ${color.primary} !important;
      }
    `}
  `}
`;

const ClearButton = styled.div`
  position: absolute;
  top: 50%;
  right: 15px;
  font-size: 0;
  transform: translateY(-50%);
  padding-top: 2.5px;
  padding-right: 2.5px;
  padding-bottom: 2.5px;
  padding-left: 2.5px;
  cursor: pointer;
  
  &:hover {
    
    svg {
      fill: ${color.primary};
    }
  }
  
  svg {
    transition: 0.1s;
  }
`;

const FieldWrapper = styled.div`
  flex-grow: 1;
  position: relative;
  margin-right: 15px;
`;

const SettingsButton = styled.div`
  cursor: pointer;
  
  &:hover {
    
    svg {
      fill: ${color.primary};
    }
  }
  
  svg {
    transition: 0.1s
  }
`;

const Wrapper = styled.div`
  position: relative;
  z-index: 1;
  display: flex;
  justify-content: space-between;
  align-items: center;
  background-color: #fbfbfb;
  border-bottom: 1px solid #f5f5f5;
  padding-top: 10px;
  padding-right: 20px;
  padding-bottom: 10px;
  padding-left: 20px;
  
  ${props => css`
    
    ${props.isDark && css`
      background-color: #25282c;
      border-bottom-color: #3d4348;
      
      ${Input} {
        color: #acacac;
        background-color: #3d4348;
        border-color: transparent;
        
        &::placeholder {
          color: #acacac;
        }
        
        &:focus {
          border-color: ${color.primary};
        }
      }
      
      ${SettingsButton} {
          
        &:hover {
        
          svg {
            fill: ${color.primary};
          }
        }
      }
    `}
  `}
`;


export class Search extends React.Component {
  handleSettingsButtonClick = () => {
    ipcRenderer.send('open-preferences-window');
  };

  handleKeyDown = (e) => {
    if (e.keyCode === 27 && this.input) {
      this.input.blur();
    }
  };

  componentDidMount = () => {
    document.addEventListener('keydown', this.handleKeyDown, true);
  };

  componentWillUnmount = () => {
    document.addEventListener('keydown', this.handleKeyDown, true);
  };

  render = () => {
    if (this.props.shouldFocusInput && this.input) {
      this.input.focus();
    }

    return (
      <Wrapper className={ this.props.className } isDark={ this.props.isDark }>
        <FieldWrapper>
          <Input
            innerRef={ (component) => this.input = component }
            placeholder={ '#link #work Design' }
            value={ this.props.value }
            isDark={ this.props.isDark }
            isFilled={ this.props.value && this.props.value.length }
            onChange={ this.props.onChange }
          />

          {
            this.props.value && this.props.value.length &&
            <ClearButton onClick={ this.props.onClearButtonClick }>
              <CloseIcon width={ 12.5 } height={ 12.5 } fill={ this.props.isDark ? '#acacac' : color.text.primary }/>
            </ClearButton>
          }
        </FieldWrapper>

        <SettingsButton onClick={ this.handleSettingsButtonClick }>
          <CogIcon width={ 24 } height={ 24 } fill={ this.props.isDark ? '#acacac' : color.text.primary }/>
        </SettingsButton>
      </Wrapper>
    );
  };
}
