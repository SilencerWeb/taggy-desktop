import * as React from 'react';
import styled, { css } from 'styled-components';

import { color } from '../theme';


const Subtitle = styled.div`
  font-size: 22px;
  padding-right: 20px;
  padding-left: 20px;
  margin-bottom: 15px;
`;

const Button = styled.div`
  font-size: 20px;
  padding-top: 10px;
  padding-right: 20px;
  padding-bottom: 10px;
  padding-left: 20px;
  margin-right: -1px;
  transition: 0.1s;
  cursor: pointer;
  
  &:hover {
    color: ${color.primary};
  }
  
  ${props => css`
    
    ${props.active && css`
      color: ${color.text.secondary} !important;
      background-color: ${color.primary};
      
        &:hover {
          color: ${color.text.secondary} !important;
        }
    `}
  `}
`;

const CategoriesSection = styled.div`
  margin-bottom: 10px;
  
  &:last-child {
    margin-bottom: 0;
  }
`;

const Categories = styled.div`
  margin-bottom: 20px;
`;

const TagName = styled.div`
  flex-shrink: 1;
  font-size: 16px;  
  white-space: nowrap; 
  text-overflow: ellipsis;
  margin-right: 5px;
  overflow: hidden;
`;

const TagCount = styled.div`
  flex-shrink: 0;
  min-width: 40px;
  font-size: 14px;
  text-align: center;
  border: 1px solid ${color.text.primary};
  border-radius: 10px;
  padding-right: 10px;
  padding-left: 10px;
`;

const Tag = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 10px;
  cursor: pointer;
  transition: 0.1s;
  
  &:last-child {
    margin-bottom: 0;
  }
  
  &:hover {
    color: ${color.primary};
    
    ${TagCount} {
      border-color: ${color.primary};
    }
  }
`;

const Tags = styled.div`
  padding-right: 20px;
  padding-left: 20px;
`;

const Content = styled.div`
  display: flex;
  flex-direction: column;
  height: 100vh;
  padding-bottom: 20px;
  overflow: auto;
`;

const Wrapper = styled.div`
  position: relative;
  z-index: 1;
  background-color: #fbfbfb;
  border-right: 1px solid #f5f5f5;
  
  ${props => css`
    
    ${props.isDark && css`
      background-color: #25282c;
      border-right-color: #3d4348;
      
      ${Button} {
        color: #acacac;
        
        &:hover {
          color: ${color.primary};
        }
      }
      
      ${Subtitle} {
        color: #acacac !important;
      }
      
      ${Tag} {
        color: #acacac;
        
        &:hover {
          color: ${color.primary};
        }
      }
      
      ${TagCount} {
        border-color: #acacac;
      }
    `}  
  `}
`;


export const Sidebar = (props) => {

  return (
    <Wrapper className={ props.className } isDark={props.isDark }>
      <Content>
        <Categories>
          <CategoriesSection>
            <Button active={ props.activeCategory === 'all' } onClick={ () => props.onCategoryClick('all') }>
              All
            </Button>

            <Button active={ props.activeCategory === 'unsorted' } onClick={ () => props.onCategoryClick('unsorted') }>
              Unsorted
            </Button>

            <Button active={ props.activeCategory === 'favorite' } onClick={ () => props.onCategoryClick('favorite') }>
              Favorite
            </Button>
          </CategoriesSection>

          <CategoriesSection>
            <Button active={ props.activeCategory === 'text' } onClick={ () => props.onCategoryClick('text') }>
              Text
            </Button>

            <Button active={ props.activeCategory === 'link' } onClick={ () => props.onCategoryClick('link') }>
              Link
            </Button>

            <Button active={ props.activeCategory === 'image' } onClick={ () => props.onCategoryClick('image') }>
              Image
            </Button>
          </CategoriesSection>
        </Categories>

        {
          props.tags && props.tags.length > 0 &&
          <React.Fragment>
            <Subtitle>Tags</Subtitle>

            <Tags>
              {
                props.tags.map((tag) => (
                  <Tag key={ tag.name } onClick={ (e) => props.onTagClick(tag.name) }>
                    <TagName>{ tag.name }</TagName>
                    <TagCount>{ tag.count }</TagCount>
                  </Tag>
                ))
              }
            </Tags>
          </React.Fragment>
        }
      </Content>
    </Wrapper>
  );
};
