const css = String.raw;


export const color = {
  primary: '#1e8aff',
  secondary: '#ffffff',
  error: '#e41717',
  text: {
    primary: '#3d4348',
    secondary: '#ffffff',
  },
};

export const globalStyles = css`
  @font-face {
    font-family: 'Open Sans';
    font-style: normal;
    font-weight: 300;
    src: local('Open Sans Light'), local('OpenSans-Light'),
         url('../static/fonts/open-sans-v15-cyrillic_latin-300.woff2') format('woff2'),
         url('../static/fonts/open-sans-v15-cyrillic_latin-300.woff') format('woff');
  }
  
  @font-face {
    font-family: 'Open Sans';
    font-style: normal;
    font-weight: 400;
    src: local('Open Sans Regular'), local('OpenSans-Regular'),
         url('../static/fonts/open-sans-v15-cyrillic_latin-regular.woff2') format('woff2'),
         url('../static/fonts/open-sans-v15-cyrillic_latin-regular.woff') format('woff');
  }
  
  @font-face {
    font-family: 'Open Sans';
    font-style: normal;
    font-weight: 600;
    src: local('Open Sans SemiBold'), local('OpenSans-SemiBold'),
         url('../static/fonts/open-sans-v15-cyrillic_latin-600.woff2') format('woff2'),
         url('../static/fonts/open-sans-v15-cyrillic_latin-600.woff') format('woff');
  }

  *,
  *:before,
  *:after {
    box-sizing: border-box;
  }

  body {
    position: relative;
    font-family: "Open Sans", sans-serif;
    font-size: 12px;
    line-height: 1.45;
    color: ${color.text.primary};
    margin-top: 0;
    margin-right: 0;
    margin-bottom: 0;
    margin-left: 0;
  }
  
  svg {
    vertical-align: middle;
  }
  
  .react-tooltip {
    font-size: 12px;
    color: ${color.text.secondary};
    background-color: ${color.secondary};
    padding-top: 5px;
    padding-right: 10px;
    padding-bottom: 5px;
    padding-left: 10px;
    
    &_dark {
      background-color: #2d3235 !important; 
    }
  }
  
  .react-select {
  
    &__control {
      min-height: 36px !important;
      font-family: "SF Pro Display", sans-serif !important;
      font-size: 12px !important;
      line-height: 1 !important;
      border: 1px solid #f5f5f5 !important;
      border-radius: 4px !important;
      box-shadow: none !important;
      outline: none !important;
      cursor: text !important;
      transition: 0.1s !important;
      
      &:hover {
        border-color: #f5f5f5 !important;
      }
      
      &--is-focused {
        border-color: ${color.primary} !important;
      
        &:hover {
          border-color: ${color.primary} !important;
        }
        
        .react-select__multi-value {
          background-color: ${color.primary} !important;
        }
        
        .react-select__multi-value__label {
          font-size: 100% !important;
          color: ${color.text.secondary} !important;
        }
        
        .react-select__multi-value__remove {
          color: ${color.text.secondary} !important;
        }
      }
    }
    
    &__input {
      color: ${color.text.primary} !important;
    }
    
    &__indicator-separator {
      display: none !important;
    }
    
    &__dropdown-indicator {
      display: none !important;
    }
    
    &__multi-value {
      background-color: #f5f5f5 !important;
      transition: 0.1s !important;
    }
    
    &__multi-value__label {
      font-size: 100% !important;
      color: ${color.text.primary} !important;
      transition: 0.1s !important;
    }
    
    &__multi-value__remove {
      color: ${color.text.primary} !important;
      cursor: pointer !important;
      transition: 0.1s !important;
      
      &:hover {
        color: ${color.text.secondary} !important;
        background-color: ${color.error} !important;
      }
    }
    
    &__menu {
      border: 1px solid ${color.primary} !important;
      box-shadow: none !important;
      margin-top: 5px !important;
      margin-bottom: 5px !important;
    }
    
    &__menu-notice {
      color: ${color.text.primary} !important;
      padding-top: 5.5px !important;
      padding-bottom: 5.5px !important;
    }
    
    &__option {
      cursor: pointer !important;
      transition: 0.1s !important;
      
      &--is-focused {
        color: ${color.text.secondary} !important;
        background-color: ${color.primary} !important;
      }
    }
    
    &__clear-indicator {
      cursor: pointer !important;
      
      &:hover {
        
        svg {
          fill: ${color.primary} !important;
        }
      }
      
      svg {
        transition: 0.1s !important;
      }
    }
  }
  
  .react-select--dark {
  
    .react-select {
    
      &__control {
        background-color: #3d4348 !important;
        border-color: transparent !important;
        
        &:hover {
          border-color: transparent !important;
        }
        
        &--is-focused {
          border-color: ${color.primary} !important;
        
          &:hover {
            border-color: ${color.primary} !important;
          }
          
          .react-select__multi-value {
            background-color: ${color.primary} !important;
          }
          
          .react-select__multi-value__label {
            font-size: 100% !important;
            color: ${color.text.secondary} !important;
          }
          
          .react-select__multi-value__remove {
            color: ${color.text.secondary} !important;
          }
        }
      }
      
      &__input {
        color: #acacac !important;
      }
      
      &__menu {
        background-color: #3d4348 !important;
      }
      
      &__multi-value {
        background-color: #2d3235 !important;
      }
      
      &__multi-value__label {
        color: #acacac !important;
      }
      
      &__multi-value__remove {
        color: #acacac !important;
      }
      
      &__menu-notice {
        color: #acacac !important;
      }
    }
  }
`;
