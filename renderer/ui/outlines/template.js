const reactDomTemplate = (code, config, state) => {
  const componentName = state.componentName.slice(3);

  return `export const ${componentName}Icon = props => ${code}`;
};


module.exports = reactDomTemplate;