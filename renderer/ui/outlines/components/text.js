export const TextIcon = (props) => (
  <svg viewBox="0 0 218 218" {...props}>
    <path d="M103 41h30v32h34V7H0v66h34V41h30v136H48v34h72v-34h-17z" />
    <path d="M118 90v42h24v-18h13v73h-10v24h46v-24h-9v-73h12v18h24V90z" />
  </svg>
);
