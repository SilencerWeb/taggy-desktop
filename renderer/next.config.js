module.exports = {
  webpack: (config) => {
    config.target = 'electron-renderer';

    return config;
  },
  exportPathMap: () => ({
    '/index': { page: '/index' },
    '/preferences': { page: '/preferences' },
    '/success-save': { page: '/success-save' },
    '/update-progress': { page: '/update-progress' },
  }),
};