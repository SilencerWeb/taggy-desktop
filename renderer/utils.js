import fast from 'fast.js';


export const getHostnameFromUrl = (url, withoutDomainZone) => {
  let hostname;

  if (url.indexOf('//') > -1) {
    hostname = url.split('/')[2];
  } else {
    hostname = url.split('/')[0];
  }

  hostname = hostname.split(':')[0];
  hostname = hostname.split('?')[0];

  if (~hostname.indexOf('www.')) {
    hostname = hostname.slice(4);
  }

  if (withoutDomainZone) {
    hostname = hostname.slice(0, hostname.lastIndexOf('.'));
  }

  return hostname;
};

export const filterCardsBySearchString = (cards, searchString, activeCategory) => {
  if (!cards) return [];

  return fast.filter(cards, (card) => {
    let isValidByTags = true;
    let isValidByWords = true;

    if (activeCategory === 'unsorted' && card.isSorted || activeCategory === 'favorite' && !card.isFavorite) {
      return false;
    }

    if (activeCategory === 'text' && card.type !== 'text' || activeCategory === 'link' && card.type !== 'link' || activeCategory === 'image' && card.type !== 'image') {
      return false;
    }

    const searchingKeywords = searchString.length > 0 ? searchString.split(' ') : null;

    if (searchingKeywords === null) return true;

    let searchingTags = searchingKeywords.filter((keyword) => keyword[0] === '#');
    let searchingWords = searchingKeywords.filter((keyword) => keyword[0] !== '#');

    fast.forEach(searchingTags, (searchingTag) => {
      if (!isValidByTags) return;

      let isValidTag = false;

      card.tags.forEach((tag) => {
        if (tag === searchingTag) {
          isValidTag = true;
        }
      });

      if (!isValidTag) {
        isValidByTags = false;
      }
    });

    fast.forEach(searchingWords, (searchingWord) => {
      if (!isValidByWords) return;

      let isValidWord = false;

      if (card.title && card.title.toLowerCase().includes(searchingWord.toLowerCase())) {
        isValidWord = true;
      }

      if (card.text && card.text.toLowerCase().includes(searchingWord.toLowerCase())) {
        isValidWord = true;
      }

      if (card.link && card.link.toLowerCase().includes(searchingWord.toLowerCase())) {
        isValidWord = true;
      }

      if (!isValidWord || card.type === 'image') { // because image type card doesn't have title or text
        isValidByWords = false;
      }
    });

    return isValidByTags && isValidByWords;
  });
};

export const rgba = (hex, alpha) => {
  const r = parseInt(hex.slice(1, 3), 16);
  const g = parseInt(hex.slice(3, 5), 16);
  const b = parseInt(hex.slice(5, 7), 16);

  return `rgba(${r}, ${g}, ${b}, ${alpha})`;
};

export const formatBytes = (bytes, decimals) => { // from https://stackoverflow.com/a/18650828/7455781
  if (bytes === 0) return '0 Bytes';

  const k = 1024;
  const dm = decimals <= 0 ? 0 : decimals || 2;
  const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
  const i = Math.floor(Math.log(bytes) / Math.log(k));

  return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
};

export const prettifyShortcut = (shortcut, asSymbols = true) => {
  shortcut = shortcut
    .replace('Control', process.platform === 'darwin' && asSymbols ? '⌃' : 'Ctrl')
    .replace('Command', asSymbols ? '⌘' : 'Command')
    .replace('Shift', process.platform === 'darwin' && asSymbols ? '⇧' : 'Shift')
    .replace('Alt', process.platform === 'darwin' && asSymbols ? '⌥' : 'Alt');

  if (process.platform === 'darwin' && asSymbols) {
    shortcut = shortcut.split('+').join('');
  }

  return shortcut;
};
