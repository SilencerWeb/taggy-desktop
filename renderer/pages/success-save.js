import * as React from 'react';
import styled from 'styled-components';

import { CheckIcon } from '../ui/outlines';

import { color } from '../ui/theme';


const Text = styled.div`
  font-size: 30px;
  font-weight: 300;
  line-height: 1;
  color: ${color.text.secondary};
  margin-top: 10px;
`;

const Wrapper = styled.div`
  width: 100%;
  height: 100vh;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;


export default class SuccessSavePage extends React.Component {

  render = () => {
    return (
      <Wrapper>
        <CheckIcon width={ 70 } height={ 70 } fill={ color.secondary }/>
        <Text>Saved</Text>
      </Wrapper>
    );
  };
}