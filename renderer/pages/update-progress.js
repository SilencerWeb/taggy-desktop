import * as React from 'react';
import styled, { css } from 'styled-components';
import { ipcRenderer } from 'electron';

import { ProgressBar } from '../ui/atoms';

import { formatBytes } from '../utils';


const Title = styled.div`
  font-size: 20px;
  font-weight: 500;
  margin-bottom: 20px;
`;

const StyledProgressBar = styled(ProgressBar)`
  margin-bottom: 10px;
`;

const Footer = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  min-height: 100vh;
  padding-top: 20px;
  padding-right: 20px;
  padding-bottom: 20px;
  padding-left: 20px;
  
  ${props => css`
    
    ${props.isDark && css`
      color: #acacac;
      background-color: #2d3235;
    `}
  `}
`;


export default class UpdateProgressPage extends React.Component {
  state = {
    progress: null,
  };

  componentDidMount = () => {
    ipcRenderer.on('send-update-progress', (event, progress) => {
      this.setState({ progress: progress });
    });

    ipcRenderer.on('send-settings', (event, settings) => {
      this.setState({ settings: settings });
    });

    ipcRenderer.send('get-settings');
  };

  render = () => {
    if (!this.state.progress) return <Wrapper isDark={ this.state.settings && this.state.settings.isDarkModeEnabled }/>;

    const { percent, transferred, total } = this.state.progress;

    return (
      <Wrapper isDark={ this.state.settings && this.state.settings.isDarkModeEnabled }>
        <Title>Downloading update...</Title>
        <StyledProgressBar width={ percent || 0 } isDark={ this.state.settings && this.state.settings.isDarkModeEnabled }/>

        <Footer>
          { formatBytes(transferred) } of { formatBytes(total) }
        </Footer>
      </Wrapper>
    );
  };
}
