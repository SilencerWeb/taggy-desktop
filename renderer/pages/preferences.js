import * as React from 'react';
import styled, { css } from 'styled-components';
import { ipcRenderer } from 'electron';

import {
  CogIcon,
  EyeAppearanceIcon,
  CommandIcon,
  QuestionMarkIcon,
  ReloadIcon,
  TelegramIcon,
  EmailIcon,
} from '../ui/outlines';

import { color } from '../ui/theme';

import { rgba, prettifyShortcut } from '../utils';


const TabText = styled.div`
  margin-top: 5px;
`;

const Tab = styled.div`
  flex-basis: 75px;
  display: flex;
  flex-direction: column;
  align-items: center;
  border-top-right-radius: 5px;
  border-top-left-radius: 5px;
  padding-top: 10px;
  padding-right: 10px;
  padding-bottom: 10px;
  padding-left: 10px;
  cursor: pointer;
  user-select: none;
  
  ${props => css`
    
    ${props.isActive && css`
      background-color: ${props.isDark ? `${rgba('#acacac', 0.1)} !important` : `${rgba(color.text.primary, 0.1)} !important`};
    `}
  `}
`;

const TabsWrapper = styled.div`
  display: flex;
  background-color: ${props => props.isDark ? '#25282c' : '#fbfbfb'};
  padding-top: 10px;
  padding-right: 5px;
  padding-left: 5px;
`;

const Label = styled.label`
  display: flex;
  align-items: center;
  min-width: 130px;
  cursor: pointer;
`;

const CheckBox = styled.input.attrs({ type: 'checkbox' })`
  font-size: 16px;
  margin-top: 0;
  margin-right: 15px;
  margin-bottom: 0;
  margin-left: 0;
  cursor: pointer;
`;

const LabelText = styled.div`
  display: inline-block;
  vertical-align: top;
  font-size: 14px;
  user-select: none;
`;

const GeneralTab = styled.div`
  flex-grow: 1;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-around;
  padding-top: 25px;
  padding-bottom: 25px;
  user-select: none;
`;

const ShortcutsTabItemText = styled.div`
  font-size: 14px;
  margin-right: 20px;
`;

const ShortcutsTabItemInput = styled.div`
  width: 150px;
  font-size: 13px;
  text-align: center;
  border-bottom: 1px solid ${color.text.primary};
  margin-right: 20px;
  cursor: pointer;
  transition: 0.1s;
  
  &:hover {
    border-bottom-color: ${color.primary};
  }
`;

const ShortcutsTabItemButton = styled.div`
  margin-right: 10px;
  cursor: pointer;
  
  &:last-child {
    margin-right: 0;
  }

  &:hover {
  
    svg {
      fill: ${color.primary};
    }
  }
  
  svg {
    transition: 0.1s;
  }
`;

const ShortcutsTabItemButtonsWrapper = styled.div`
  display: flex;
  align-items: center;
`;

const ShortcutsTabItem = styled.div`
  display: flex;
  align-items: center;
`;

const ShortcutsTab = styled.div`
  flex-grow: 1;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-around;
  padding-top: 25px;
  padding-bottom: 25px;
  user-select: none;
`;

const Button = styled.button`
  background: none;
  border: 1px solid #f5f5f5;
  border-radius: 4px;
  padding-top: 5px;
  padding-right: 10px;
  padding-bottom: 5px;
  padding-left: 10px;
  outline: none;
  cursor: pointer;
  transition: 0.1s;
  margin-left: 10px;

  &:hover {
    border-color: ${color.primary};
  }
`;

const ContactMeButton = styled.a`
  display: inline-block;
  text-decoration: none;
  margin-left: 10px;
  cursor: pointer;
  
  &:hover {
    
    svg {
      fill: ${color.primary} 
    }
  }
  
  svg {
    transition: 0.1s;
  }
`;

const AboutTabItem = styled.div`
  display: flex;
  align-items: center;
`;

const AboutTab = styled.div`
  flex-grow: 1;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-around;
  font-size: 14px;
  padding-top: 25px;
  padding-bottom: 25px;
`;

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  min-height: 100vh;
  
  ${props => css`
    
    ${props.isDark && css`
      color: #acacac;
      background-color: #2d3235;
     
      ${ShortcutsTabItemInput} {
        border-bottom-color: #acacac;
  
        &:hover {
          border-bottom-color: ${color.primary};
        }
      }
      
      ${Button} {
        color: #acacac;
        border-color: #3d4348;
        
        &:hover {
          border-color: ${color.primary};
        }
      }
    `}
  `}
`;


export default class PreferencesPage extends React.Component {
  state = {
    activeTab: 'general',
  };

  handleTabClick = (activeTab) => {
    this.setState({ activeTab: activeTab });
  };

  handleCheckBoxClick = (setting) => {

    this.setState((prevState) => {
      const settings = {
        ...prevState.settings,
        [setting]: !prevState.settings[setting],
      };

      ipcRenderer.send('change-settings', settings);

      return {
        ...prevState,
        settings: settings,
      };
    });
  };

  handleShortcutItemInputClick = (shortcutName) => {
    this.setState((prevState) => {
      const newShortcuts = prevState.shortcuts.map((shortcut) => {
        if (shortcut.name === shortcutName) {
          shortcut.isRecording = !shortcut.isRecording;
        } else {
          shortcut.isRecording = false;
        }

        return shortcut;
      });

      return {
        ...prevState,
        shortcuts: newShortcuts,
      };
    });
  };

  handleKeyDown = (e) => {
    if (!this.state.shortcuts.find((shortcut) => shortcut.isRecording)) return;

    let shortcut = [];

    if (e.ctrlKey) {
      shortcut.push('Control');
    }

    if (e.metaKey) {
      shortcut.push('Command');
    }

    if (e.shiftKey) {
      shortcut.push('Shift');
    }

    if (e.altKey) {
      shortcut.push('Alt');
    }

    if (shortcut.length === 0 || shortcut.length > 2) return;

    if (e.keyCode < 65 || e.keyCode > 90) return;

    shortcut.push(String.fromCharCode(e.keyCode).toUpperCase());

    shortcut = shortcut.join('+');

    this.setState((prevState) => {
      const recordingShortcut = prevState.shortcuts.find((shortcut) => shortcut.isRecording);

      const newShortcuts = prevState.shortcuts.map((shortcut) => {
        shortcut.isRecording = false;

        return shortcut;
      });

      ipcRenderer.send('save-shortcut', {
        name: recordingShortcut.name,
        accelerator: shortcut,
      });

      return {
        ...prevState,
        shortcuts: newShortcuts,
      };
    });
  };

  handleResetShortcutButtonClick = (name) => {
    ipcRenderer.send('reset-shortcut', name);
  };

  handleCheckForUpdatesButtonClick = () => {
    ipcRenderer.send('check-for-updates');
  };

  componentDidMount = () => {

    ipcRenderer.on('stop-recording-shortcut', () => {
      this.setState((prevState) => {
        const newShortcuts = prevState.shortcuts.map((shortcut) => {
          shortcut.isRecording = false;

          return shortcut;
        });

        return {
          ...prevState,
          shortcuts: newShortcuts,
        };
      });
    });

    ipcRenderer.on('send-shortcuts', (event, shortcuts) => {
      this.setState({ shortcuts: shortcuts });
    });

    ipcRenderer.on('send-settings', (event, settings) => {
      this.setState({ settings: settings });
    });

    ipcRenderer.on('send-app-version', (event, appVersion) => {
      this.setState({ appVersion: appVersion });
    });

    ipcRenderer.send('get-shortcuts');
    ipcRenderer.send('get-settings');
    ipcRenderer.send('get-app-version');

    document.addEventListener('keydown', this.handleKeyDown, true);
  };

  componentWillUnmount = () => {
    document.addEventListener('keydown', this.handleKeyDown, true);
  };

  render = () => {

    return (
      <Wrapper isDark={ this.state.settings && this.state.settings.isDarkModeEnabled }>
        <TabsWrapper isDark={ this.state.settings && this.state.settings.isDarkModeEnabled }>
          <Tab
            isActive={ this.state.activeTab === 'general' }
            isDark={ this.state.settings && this.state.settings.isDarkModeEnabled }
            onClick={ () => this.handleTabClick('general') }
          >
            <CogIcon width={ 25 } height={ 25 } fill={ this.state.settings && this.state.settings.isDarkModeEnabled ? '#acacac' : color.text.primary }/>
            <TabText>General</TabText>
          </Tab>

          <Tab
            isActive={ this.state.activeTab === 'appearance' }
            isDark={ this.state.settings && this.state.settings.isDarkModeEnabled }
            onClick={ () => this.handleTabClick('appearance') }
          >
            <EyeAppearanceIcon width={ 25 } height={ 25 } fill={ this.state.settings && this.state.settings.isDarkModeEnabled ? '#acacac' : color.text.primary }/>
            <TabText>Appearance</TabText>
          </Tab>

          <Tab
            isActive={ this.state.activeTab === 'shortcuts' }
            isDark={ this.state.settings && this.state.settings.isDarkModeEnabled }
            onClick={ () => this.handleTabClick('shortcuts') }
          >
            <CommandIcon width={ 25 } height={ 25 } fill={ this.state.settings && this.state.settings.isDarkModeEnabled ? '#acacac' : color.text.primary }/>
            <TabText>Shortcuts</TabText>
          </Tab>

          <Tab
            isActive={ this.state.activeTab === 'about' }
            isDark={ this.state.settings && this.state.settings.isDarkModeEnabled }
            onClick={ () => this.handleTabClick('about') }
          >
            <QuestionMarkIcon width={ 25 } height={ 25 } fill={ this.state.settings && this.state.settings.isDarkModeEnabled ? '#acacac' : color.text.primary }/>
            <TabText>About</TabText>
          </Tab>
        </TabsWrapper>

        {
          this.state.activeTab === 'general' &&
          <GeneralTab>
            <Label>
              <CheckBox
                checked={ this.state.settings && this.state.settings.isLaunchAtLoginEnabled }
                onChange={ () => this.handleCheckBoxClick('isLaunchAtLoginEnabled') }
              />
              <LabelText>Launch at login</LabelText>
            </Label>

            <Label>
              <CheckBox
                checked={ this.state.settings && this.state.settings.isAutoUpdatingEnabled }
                onChange={ () => this.handleCheckBoxClick('isAutoUpdatingEnabled') }
              />
              <LabelText>Auto updating</LabelText>
            </Label>
          </GeneralTab>
        }

        {
          this.state.activeTab === 'appearance' &&
          <GeneralTab>
            <Label>
              <CheckBox
                checked={ this.state.settings && this.state.settings.isDarkModeEnabled }
                onChange={ () => this.handleCheckBoxClick('isDarkModeEnabled') }
              />
              <LabelText>Enable dark mode</LabelText>
            </Label>
          </GeneralTab>
        }

        {
          this.state.activeTab === 'shortcuts' &&
          <ShortcutsTab>
            {
              this.state.shortcuts.map((shortcut) => {
                return (
                  <ShortcutsTabItem key={ shortcut.name }>
                    <ShortcutsTabItemText>{ shortcut.name === 'save-item' && 'Save item:' }</ShortcutsTabItemText>

                    <ShortcutsTabItemInput onClick={ () => this.handleShortcutItemInputClick(shortcut.name) }>
                      { shortcut.isRecording ? 'Recording' : (shortcut.accelerator ? prettifyShortcut(shortcut.accelerator) : 'Not set') }
                    </ShortcutsTabItemInput>

                    <ShortcutsTabItemButtonsWrapper>
                      <ShortcutsTabItemButton onClick={ () => this.handleResetShortcutButtonClick(shortcut.name) }>
                        <ReloadIcon width={ 15 } height={ 15 } fill={ this.state.settings && this.state.settings.isDarkModeEnabled ? '#acacac' : color.text.primary }/>
                      </ShortcutsTabItemButton>
                    </ShortcutsTabItemButtonsWrapper>
                  </ShortcutsTabItem>
                );
              })
            }
          </ShortcutsTab>
        }

        {
          this.state.activeTab === 'about' &&
          <AboutTab>
            <AboutTabItem>
              App version: { this.state.appVersion }
              <Button onClick={ this.handleCheckForUpdatesButtonClick }>Check for updates</Button>
            </AboutTabItem>

            <AboutTabItem>
              Want to contact me?

              <ContactMeButton href={ 'tg://resolve?domain=thisbabeisactuallynaked' }>
                <TelegramIcon width={ 16 } height={ 16 } fill={ this.state.settings && this.state.settings.isDarkModeEnabled ? '#acacac' : color.text.primary }/>
              </ContactMeButton>

              <ContactMeButton href={ 'mailto:maksim@taggy.pro' }>
                <EmailIcon width={ 16 } height={ 16 } fill={ this.state.settings && this.state.settings.isDarkModeEnabled ? '#acacac' : color.text.primary }/>
              </ContactMeButton>
            </AboutTabItem>
          </AboutTab>
        }
      </Wrapper>
    );
  };
}
