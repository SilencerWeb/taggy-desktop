import * as React from 'react';
import styled, { css } from 'styled-components';
import { ipcRenderer } from 'electron';

import { Sidebar, Search, Card, CardViewer } from '../ui/molecules';

import { SadFaceIcon } from '../ui/outlines';

import { filterCardsBySearchString, prettifyShortcut } from '../utils';


const StyledSidebar = styled(Sidebar)`
  flex-grow: 1;
  width: 20%;
`;

const CardList = styled.div`
  flex: 1 0 25%;
  display: flex;
  flex-wrap: wrap;
  max-height: calc(100vh - 60px);
  overflow: auto;
  
  ${props => css`
    
    ${props.isCardViewerActive && css`
      display: block;
    `}
  `}
`;

const StyledCardViewer = styled(CardViewer)`
  width: 75%;
  height: calc(100vh - 60px);
`;

const Content = styled.div`
  display: flex;
`;

const NotFoundText = styled.div`
  font-size: 65px;
  font-weight: 300;
  text-align: center;
  margin-top: 30px;
`;

const NotFound = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  height: calc(100vh - 60px);
  color: #bebfc3;
`;

const StartText = styled.div`
  font-size: 45px;
  font-weight: 300;
  text-align: center;
  margin-top: 30px;
`;

const Start = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  height: calc(100vh - 60px);
  color: #bebfc3;
`;

const Main = styled.div`
  flex-grow: 1;
  width: 80%;
`;

const Wrapper = styled.div`
  display: flex;
  width: 100%;
  
  ${props => css`
    
    ${props.isDark && css`
      background-color: #2d3235;
      
      ${NotFound}, ${Start} {
        color: #acacac;
      }
    `}
  `}
`;


export default class IndexPage extends React.PureComponent {
  state = {
    activeCategory: 'all',
    user: null,
    cards: null,
    filteredCards: null,
    activeCard: null,
    isCardEditing: false,
    isCardViewerActive: false,
    searchingString: '',
    filterDelayTimeout: null,
  };

  filterCards = () => {
    this.setState((prevState) => {
      const filteredCards = filterCardsBySearchString(prevState.cards, prevState.searchingString, prevState.activeCategory);

      return {
        ...prevState,
        filteredCards: filteredCards && filteredCards.reverse(),
      };
    });
  };

  handleCategoryClick = (activeCategory) => {
    this.setState({
      activeCategory: activeCategory,
      activeCard: null,
      isCardEditing: false,
      isCardViewerActive: false,
    }, this.filterCards);
  };

  handleTagClick = (tag) => {
    this.setState({
      activeCard: null,
      isCardEditing: false,
      isCardViewerActive: false,
      searchingString: tag,
    }, this.filterCards);
  };

  handleSearchChange = (e) => {

    if (this.state.cards.length > 100) {
      clearTimeout(this.state.filterDelayTimeout);

      const filterDelayTimeout = setTimeout(() => {
        this.filterCards();
      }, 500);

      this.setState({
        activeCard: null,
        isCardEditing: false,
        isCardViewerActive: false,
        searchingString: e.currentTarget.value,
        filterDelayTimeout: filterDelayTimeout,
      });
    } else {
      this.setState({
        activeCard: null,
        isCardEditing: false,
        isCardViewerActive: false,
        searchingString: e.currentTarget.value,
      }, this.filterCards);
    }
  };

  handleSearchCleanButtonClick = () => {
    this.setState({ searchingString: '' }, this.filterCards);
  };

  handleCardViewerCloseButtonClick = () => {
    this.setState({
      activeCard: null,
      isCardEditing: false,
      isCardViewerActive: false,
    });
  };

  handleCardEditButtonClick = (activeCard) => {
    this.setState({
      activeCard: activeCard,
      isCardEditing: true,
      isCardViewerActive: true,
    });
  };

  handleCardOpenButtonClick = (activeCard) => {
    if (activeCard && activeCard.type === 'link' && activeCard.link) {
      ipcRenderer.send('open-link', activeCard.link);

      return;
    }

    this.setState({
      activeCard: activeCard,
      isCardEditing: false,
      isCardViewerActive: true,
      searchingString: '',
    });
  };

  handleCardViewerEditButtonClick = (activeCard) => {
    this.setState({
      activeCard: activeCard,
      isCardEditing: true,
      isCardViewerActive: true,
    });
  };

  handleCardViewerCancelButtonClick = (isLink) => {
    this.setState((prevState) => ({
      ...prevState,
      activeCard: isLink ? null : prevState.activeCard,
      isCardEditing: false,
      isCardViewerActive: isLink ? false : true,
    }));
  };

  handleCardViewerSaveButtonClick = (isLink) => {
    this.setState((prevState) => ({
      ...prevState,
      activeCard: isLink ? null : prevState.activeCard,
      isCardEditing: false,
      isCardViewerActive: isLink ? false : true,
    }));
  };

  componentDidMount = () => {
    ipcRenderer.on('console.log', (event, message) => {
      console.log(message);
    });

    ipcRenderer.on('close-card-viewer', () => {
      this.setState({
        activeCard: null,
        isCardEditing: false,
        isCardViewerActive: false,
      });
    });

    ipcRenderer.on('focus-search', () => {
      this.setState({ shouldFocusInput: true });

      setTimeout(() => {
        this.setState({ shouldFocusInput: false });
      }, 100);
    });

    ipcRenderer.on('send-tags', (event, tags) => {
      this.setState({ tags: tags });
    });

    ipcRenderer.on('send-user', (event, user) => {
      this.setState({ user: user });
    });

    ipcRenderer.on('send-cards', (event, cards) => {
      this.setState({ cards: cards }, () => {
        this.filterCards();

        if (!this.state.activeCard) return;

        const activeCard = this.state.cards.find((card) => {
          return card.id === this.state.activeCard.id;
        });

        this.setState({ activeCard: activeCard });
      });
    });

    ipcRenderer.on('send-shortcuts', (event, shortcuts) => {
      this.setState({ shortcuts: shortcuts });
    });

    ipcRenderer.on('send-settings', (event, settings) => {
      this.setState({ settings: settings });
    });

    ipcRenderer.send('get-user');
    ipcRenderer.send('get-tags');
    ipcRenderer.send('get-cards');
    ipcRenderer.send('get-shortcuts');
    ipcRenderer.send('get-settings');
  };

  render = () => {

    return (
      <Wrapper isDark={ this.state.settings && this.state.settings.isDarkModeEnabled }>
        <StyledSidebar
          user={ this.state.user }
          cards={ this.state.cards }
          tags={ this.state.tags }
          activeCategory={ this.state.activeCategory }
          isDark={ this.state.settings && this.state.settings.isDarkModeEnabled }
          onCategoryClick={ this.handleCategoryClick }
          onTagClick={ this.handleTagClick }
        />

        <Main>
          <Search
            value={ this.state.searchingString }
            shouldFocusInput={ this.state.shouldFocusInput }
            isDark={ this.state.settings && this.state.settings.isDarkModeEnabled }
            onChange={ this.handleSearchChange }
            onClearButtonClick={ this.handleSearchCleanButtonClick }
          />

          {
            this.state.filteredCards && !!this.state.filteredCards.length > 0 &&
            <Content>
              <CardList isCardViewerActive={ this.state.isCardViewerActive && this.state.activeCard }>
                {
                  this.state.filteredCards.map((card) => {
                    return (
                      <Card
                        card={ card }
                        isDark={ this.state.settings && this.state.settings.isDarkModeEnabled }
                        isActive={ this.state.activeCard && card.id === this.state.activeCard.id }
                        isCardViewerActive={ this.state.isCardViewerActive && this.state.activeCard }
                        onEditButtonClick={ this.handleCardEditButtonClick }
                        onOpenButtonClick={ this.handleCardOpenButtonClick }
                        onTagClick={ this.handleTagClick }
                        key={ card.id }
                      />
                    );
                  })
                }
              </CardList>

              {
                this.state.isCardViewerActive && this.state.activeCard &&
                <StyledCardViewer
                  card={ this.state.activeCard }
                  tags={ this.state.tags }
                  isDark={ this.state.settings && this.state.settings.isDarkModeEnabled }
                  onCloseButtonClick={ this.handleCardViewerCloseButtonClick }
                  onEditButtonClick={ this.handleCardViewerEditButtonClick }
                  onCancelButtonClick={ this.handleCardViewerCancelButtonClick }
                  onSaveButtonClick={ this.handleCardViewerSaveButtonClick }
                  onTagClick={ this.handleTagClick }
                  isEditing={ this.state.isCardEditing }
                />
              }
            </Content>
          }

          {
            this.state.cards && this.state.cards.length > 0 &&
            (!this.state.filteredCards || this.state.filteredCards.length === 0) &&
            <NotFound>
              <SadFaceIcon width={ 100 } height={ 100 } fill={ this.state.settings && this.state.settings.isDarkModeEnabled ? '#acacac' : '#bebfc3' }/>
              <NotFoundText>No results found</NotFoundText>
            </NotFound>
          }

          {
            (!this.state.cards || this.state.cards.length === 0) &&
            <Start>
              <StartText>
                1. Copy something you want to save
                <br/>
                2. Press shortcut { this.state.shortcuts && prettifyShortcut(this.state.shortcuts.find((shortcut) => shortcut.name === 'save-item').accelerator, false) }
                <br/>
                <br/>
                That's all ^^
              </StartText>
            </Start>
          }
        </Main>
      </Wrapper>
    );
  };
}