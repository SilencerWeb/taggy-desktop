export const cards = [
  {
    id: 0,
    type: 'text',
    text: 'Alternatively one can use paddle.com - They charge a higher fee, tho.',
    tags: ['#text'],
  },
  {
    id: 1,
    type: 'text',
    label: 'Essential YC Advice',
    text: `1. Запускайся как можно раньше. Запустить посредственный продукт и постепенно изменять его через общение с пользователями лучше, чем в одиночку доводить свой проект до «идеального».
2. Работай над тем, что действительно нужно людям.
3. Делай вещи, которые не масштабируются.
4. Найди решение «90/10». 90/10 — это когда ты можешь на 90% решить проблему пользователя, приложив всего 10% усилий.
5. Найди 10-100 пользователей, которым по-настоящему нужен твой продукт. Маленькая группа людей, которые тебя любят, лучше большой группы, которым ты просто нравишься.
6. На каком-то этапе каждый стартап имеет нерешенные проблемы. Успех определяется не тем, есть ли у тебя изначально такие проблемы, а что ты делаешь, чтобы их как можно быстрее решить.
7. Не масштабируй свою команду и продукт, пока ты не построил что-то, что нужно людям.
8. Стартапы в любой момент времени могут хорошо решать только одну проблему.
9. Игнорируй своих конкурентов. В мире стартапов ты скорее умрешь от суицида, чем от убийства.
10. Будь хорошим человеком. Ну или хотя бы просто не будь придурком.
11. Спи достаточное количество времени и занимайся спортом.

Многие пункты подробно расписаны здесь:
http://blog.ycombinator.com/ycs-essential-startup-advice/`,
    tags: ['#text', '#advice', '#launch', '#startup_hero', '#telegram_channel'],
  },
  {
    id: 2,
    type: 'text',
    text: `
If I was you, I'd start trying to build a small following.
it helps a lot (especially if your making products for makers)
Indie Hackers / Reddit Startup + Side Project great places ...
And slowly but surely you'll build twitter + email list.
follow @AndreyAzimov example! with his medium posts + twitter`,
    tags: ['#text', '#advice'],
  },
  {
    id: 3,
    type: 'link',
    label: 'Paddle',
    text: 'Like Stripe but maybe avaliable in Uzbekistan and Russia',
    link: 'https://paddle.com/',
    tags: ['#link', '#payment', '#payment_service'],
  },
  {
    id: 4,
    type: 'link',
    label: 'How to Test and Validate Startup Ideas: – The Startu...',
    text: 'One of the first steps to launching a successful startup is building your Minimal Viable Product. In essence, a Minimal Viable Product (MVP) is all about verifying the fact that there is a market…',
    link: 'https://medium.com/swlh/how-to-start-a-startup-e4f002ff3ee1',
    tags: ['#link', '#article', '#medium', '#read_later', '#validating', '#validating_idea'],
  },
  {
    id: 5,
    type: 'link',
    label: 'The best Slack groups for UX designers – UX Collect...',
    text: 'Tons of companies are using Slack to organize and facilitate how their employees communicate on a daily basis. Slack has now more than 5 million daily active users and more than 60,000 teams around…',
    link: 'https://uxdesign.cc/the-best-slack-groups-for-ux-designers-25c621673d9c',
    tags: ['#link', '#article', '#medium', '#read_later', '#ux', '#community'],
  },
  {
    id: 6,
    type: 'image',
    link: 'static/image1.jpeg',
    tags: ['#image', '#unsplash', '#autumn', '#landscape'],
  },
  {
    id: 7,
    type: 'image',
    link: 'static/image2.jpeg',
    tags: ['#image', '#unsplash', '#sea', '#water'],
  },
];
