const { dialog } = require('electron');
const { autoUpdater } = require('electron-updater');
const isDev = require('electron-is-dev');

const { getWindow } = require('./utils');


let updater;

autoUpdater.autoDownload = false;


autoUpdater.on('update-available', () => {

  dialog.showMessageBox({
    message: `Update is available :)`,
    buttons: ['Download', 'Remind me later'],
  }, (buttonIndex) => {

    if (buttonIndex === 0) {
      autoUpdater.downloadUpdate();
    } else if (updater) {
      updater.enabled = true;
      updater = null;
    }
  });
});

autoUpdater.on('update-not-available', () => {

  if (updater) {
    dialog.showMessageBox({
      message: 'Current version is up-to-date :)',
    });

    updater.enabled = true;
    updater = null;
  }
});

autoUpdater.on('download-progress', (progress) => {
    const updateProgressWindow = getWindow('update-progress');

    if (updateProgressWindow) {

      if (!updateProgressWindow.isVisible()) {
        updateProgressWindow.showInactive();
      }

      updateProgressWindow.webContents.send('send-update-progress', progress);
    }
  },
);

autoUpdater.on('update-downloaded', () => {
  const updateProgressWindow = getWindow('update-progress');

  if (updateProgressWindow) {
    updateProgressWindow.destroy();
  }

  dialog.showMessageBox({
    message: 'Update is successfully downloaded :)',
    buttons: ['Install', 'Cancel'],
  }, (buttonIndex) => {

    if (buttonIndex === 0) {
      autoUpdater.quitAndInstall();
    } else if (updater) {
      updater.enabled = true;
      updater = null;
    }
  });
});


const checkForUpdates = (menuItem) => {

  if (!isDev) {

    if (menuItem) {
      updater = menuItem;
      updater.enabled = false;
    }

    autoUpdater.checkForUpdates();
  }
};


module.exports = { checkForUpdates };
