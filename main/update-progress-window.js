const { BrowserWindow } = require('electron');

const { getPagePath } = require('./utils');


const createUpdateProgressWindow = () => {
  const updateProgressWindow = new BrowserWindow({
    width: 400,
    height: 150,
    resizable: false,
    minimizable: false,
    maximizable: false,
    closable: false,
    fullscreenable: false,
    title: 'Updating Taggy',
    show: false,
  });

  updateProgressWindow.loadURL(getPagePath('update-progress'));

  return updateProgressWindow;
};


module.exports = { createUpdateProgressWindow };