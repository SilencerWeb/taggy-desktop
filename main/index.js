require('dotenv').config();

const {
  app,
  globalShortcut,
} = require('electron');
const prepareNext = require('electron-next');
const isDev = require('electron-is-dev');
const ua = require('universal-analytics');

const { store } = require('./database');
const { createMainWindow } = require('./main-window');
const { createPreferencesWindow } = require('./preferences-window');
const { createSuccessWindow } = require('./success-window');
const { createUpdateProgressWindow } = require('./update-progress-window');
const { createTray, destroyTray } = require('./tray');
const { createApplicationMenu } = require('./menu');
const { registerIpcMainEvents } = require('./ipc-main');
const { registerGlobalShortcuts } = require('./global-shortcuts');
const { configureSettingsAtFirstTime, getIsAutoUpdatingEnabled } = require('./settings');
const { checkForUpdates } = require('./updater');
const { getUser, createUser, normalizeUser } = require('./user');
const { getWindow } = require('./utils');


if (store.get('isLaunchedFirstTime') === undefined) {
  configureSettingsAtFirstTime();
}

if (getUser()) {
  normalizeUser();
} else {
  createUser();
}


app.on('ready', async() => {
  await prepareNext('./renderer');

  if (!isDev) {
    const visitor = ua('UA-127059931-1', getUser().id);
    visitor.pageview('/').send();
  }

  let shouldMainWindowBeVisible = true;

  if (process.platform === 'darwin' && app.getLoginItemSettings().wasOpenedAtLogin && app.getLoginItemSettings().wasOpenedAsHidden) {
    shouldMainWindowBeVisible = false;

    if (process.platform === 'darwin') {
      app.dock.hide();
    }
  }

  if (process.platform !== 'darwin') {
    shouldMainWindowBeVisible = store.get('isLaunchedFirstTime') === undefined;
  }

  createMainWindow(shouldMainWindowBeVisible);
  createPreferencesWindow();
  createSuccessWindow();
  createUpdateProgressWindow();

  createApplicationMenu();

  registerIpcMainEvents();
  registerGlobalShortcuts();

  if (!shouldMainWindowBeVisible) {
    createTray();
  }

  if (isDev) {
    const { default: installExtension, REACT_DEVELOPER_TOOLS } = require('electron-devtools-installer');

    installExtension(REACT_DEVELOPER_TOOLS);
  }

  if (getIsAutoUpdatingEnabled()) {
    checkForUpdates();
  }

  if (store.get('isLaunchedFirstTime') === undefined) {
    store.set('isLaunchedFirstTime', false);
  }
});

app.on('before-quit', () => {
  const mainWindow = getWindow('main');
  const preferencesWindow = getWindow('preferences');
  const successWindow = getWindow('success');
  const updateProgressWindow = getWindow('update-progress');

  if (mainWindow) {
    mainWindow.destroy();
  }

  if (preferencesWindow) {
    preferencesWindow.destroy();
  }

  if (successWindow) {
    successWindow.destroy();
  }

  if (updateProgressWindow) {
    updateProgressWindow.destroy();
  }

  destroyTray();
});

app.on('window-all-closed', () => {
  app.hide();

  createTray();
});

app.on('will-quit', () => {
  globalShortcut.unregisterAll();
});

app.on('quit', () => {
  app.quit();
});

app.on('activate', () => {
  const mainWindow = getWindow('main');

  if (mainWindow && !mainWindow.isVisible()) {
    destroyTray();

    mainWindow.show();
  }
});
