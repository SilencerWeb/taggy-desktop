const uuid = require('uuid/v4');
const fs = require('graceful-fs');

const { store } = require('./database');
const { showSuccessWindowAndThenHide } = require('./success-window');
const { getWindow, countTags } = require('./utils');


const getTags = () => {
  const tags = store.get('tags');

  if (!tags) {
    return countAndSaveTags();
  }

  return tags;
};

const countAndSaveTags = () => {
  const tags = countTags(getCards());
  
  store.set('tags', tags);

  sendTagsToMainWindow();
};

const getCards = () => {
  return store.get('cards') || [];
};

const getCard = (cardId) => {
  return getCards().find((card) => card.id === cardId);
};

const changeCards = (cards) => {
  store.set('cards', cards);

  countAndSaveTags();

  sendCardsToMainWindow();
};

const createCard = (card, shouldShowSuccessWindow = true) => {
  const cards = getCards();

  const newCard = {
    id: uuid(),
    type: card.type,
    tags: card.tags,
    isSorted: false,
    isFavorite: false,
    createdAt: new Date(),
    updatedAt: new Date(),
  };

  if (newCard.type === 'link') {
    newCard.title = card.title || '';
    newCard.text = card.text || '';
    newCard.link = card.link || '';
  } else if (newCard.type === 'image') {
    newCard.imagePath = card.imagePath || '';
  } else {
    newCard.title = card.title || '';
    newCard.text = card.text || '';
  }

  const isCardDuplicate = cards.find((card) => {
    if (newCard.type === 'link') {
      return card.type === newCard.type && card.link === newCard.link;
    } else if (newCard.type === 'image') {
      return card.type === newCard.type && card.imagePath === newCard.imagePath;
    } else {
      return card.type === newCard.type && card.text === newCard.text;
    }
  });

  if (isCardDuplicate) return;

  cards.push(newCard);

  changeCards(cards);

  if (shouldShowSuccessWindow) {
    showSuccessWindowAndThenHide();
  }
};

const editCard = (editedCard) => {
  let cards = getCards();

  let cardIndexForSaving = cards.findIndex((card) => {
    return card.id === editedCard.id;
  });

  cards.splice(cardIndexForSaving, 1, Object.assign(cards[cardIndexForSaving], editedCard));

  changeCards(cards);
};

const deleteCard = (cardId) => {
  let cards = getCards();

  const cardIndexForDeleting = cards.findIndex((card) => {
    return card.id === cardId;
  });

  const card = cards[cardIndexForDeleting];

  if (card.type === 'image' && card.imagePath) {
    fs.unlink(card.imagePath, (error) => {
      if (error) return;

      cards.splice(cardIndexForDeleting, 1);

      changeCards(cards);
    });
  } else {
    cards.splice(cardIndexForDeleting, 1);

    changeCards(cards);
  }
};

const sendTagsToMainWindow = () => {
  const mainWindow = getWindow('main');

  if (mainWindow) {
    mainWindow.webContents.send('send-tags', getTags());
  }
};

const sendCardsToMainWindow = () => {
  const mainWindow = getWindow('main');

  if (mainWindow) {
    mainWindow.webContents.send('send-cards', getCards());
  }
};


module.exports = {
  getTags,
  countAndSaveTags,

  getCards,

  getCard,
  createCard,
  editCard,
  deleteCard,

  sendTagsToMainWindow,
  sendCardsToMainWindow,
};