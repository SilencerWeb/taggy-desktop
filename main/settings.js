const { app } = require('electron');

const { store } = require('./database');
const { checkForUpdates } = require('./updater');
const { getWindow } = require('./utils');


const getIsLaunchAtLoginEnabled = () => {
  return store.get('isLaunchAtLoginEnabled');
};

const getIsAutoUpdatingEnabled = () => {
  return store.get('isAutoUpdatingEnabled');
};

const getIsDarkModeEnabled = () => {
  return store.get('isDarkModeEnabled');
};

const getSettings = () => ({
  isLaunchAtLoginEnabled: getIsLaunchAtLoginEnabled(),
  isAutoUpdatingEnabled: getIsAutoUpdatingEnabled(),
  isDarkModeEnabled: getIsDarkModeEnabled(),
});

const changeSetting = (setting, value, shouldSendSettingToWindows) => {
  store.set(setting, value);

  configureSettings(shouldSendSettingToWindows);
};

const configureSettings = (shouldSendSettingToWindows) => {
  if (getIsLaunchAtLoginEnabled()) {
    app.setLoginItemSettings({
      openAtLogin: true,
      openAsHidden: true,
    });
  } else {
    app.setLoginItemSettings({
      openAtLogin: false,
      openAsHidden: false,
    });
  }

  if (getIsAutoUpdatingEnabled()) {
    checkForUpdates();
  }

  if (shouldSendSettingToWindows) {
    sendSettingsToMainWindow();
    sendSettingsToPreferencesWindow();
    sendSettingsToUpdateProgressWindow();
  }
};

const configureSettingsAtFirstTime = () => {
  changeSetting('isAutoUpdatingEnabled', true);
  changeSetting('isLaunchAtLoginEnabled', true);
  changeSetting('isDarkModeEnabled', false);

  app.setLoginItemSettings({
    openAtLogin: true,
    openAsHidden: true,
  });
};

const sendSettingsToMainWindow = () => {
  const mainWindow = getWindow('main');

  if (mainWindow) {
    mainWindow.webContents.send('send-settings', getSettings());
  }
};

const sendSettingsToPreferencesWindow = () => {
  const preferencesWindow = getWindow('preferences');

  if (preferencesWindow) {
    preferencesWindow.webContents.send('send-settings', getSettings());
  }
};

const sendSettingsToUpdateProgressWindow = () => {
  const updateProgressWindow = getWindow('update-progress');

  if (updateProgressWindow) {
    updateProgressWindow.webContents.send('send-settings', getSettings());
  }
};


module.exports = {
  getIsLaunchAtLoginEnabled,
  getIsAutoUpdatingEnabled,
  getIsDarkModeEnabled,
  getSettings,

  changeSetting,

  configureSettings,
  configureSettingsAtFirstTime,

  sendSettingsToMainWindow,
  sendSettingsToPreferencesWindow,
  sendSettingsToUpdateProgressWindow,
};