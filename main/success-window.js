const { app, BrowserWindow } = require('electron');

const { getPagePath, getWindow } = require('./utils');


const createSuccessWindow = () => {
  const successWindow = new BrowserWindow({
    width: 150,
    height: 150,
    resizable: false,
    movable: false,
    focusable: false,
    alwaysOnTop: true,
    skipTaskbar: false,
    title: 'Saved',
    show: false,
    frame: false,
    backgroundColor: '#99393e41',
    hasShadow: false,
    transparent: true,
  });

  successWindow.setIgnoreMouseEvents(true);

  successWindow.loadURL(getPagePath('success-save'));

  return successWindow;
};

const showSuccessWindowAndThenHide = () => {
  const mainWindow = getWindow('main');
  const successWindow = getWindow('success');

  if (mainWindow && !mainWindow.isFullScreen() && successWindow && !successWindow.isVisible()) {
    let isDockVisible = false;

    if (process.platform === 'darwin') {
      isDockVisible = app.dock.isVisible();

      if (isDockVisible) app.dock.hide();
    }

    successWindow.showInactive();

    setTimeout(() => {

      if (successWindow) {
        successWindow.hide();

        if (process.platform === 'darwin') {
          if (isDockVisible) app.dock.show();
        }
      }
    }, 750);
  }
};


module.exports = {
  createSuccessWindow,
  showSuccessWindowAndThenHide,
};