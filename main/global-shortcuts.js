const { app, globalShortcut, clipboard } = require('electron');
const needle = require('needle');
const fs = require('graceful-fs');
const uuid = require('uuid/v4');

const { createCard } = require('./cards');
const { getShortcut } = require('./shortcuts');
const {
  getContentType,
  isImageUrl,
  getHostnameFromUrl,
  parseTag,
  parseMetaTag,
  replaceHTMLCharactersWithNormal,
} = require('./utils');


const registerGlobalShortcuts = () => {
  globalShortcut.unregisterAll();

  const saveItemAccelerator = getShortcut('save-item').accelerator;

  globalShortcut.register(saveItemAccelerator, () => {
    const content = clipboard.readText().trim();
    const image = clipboard.readImage();

    if (!content && !image) return;

    const contentType = content ? getContentType(content) : 'image';

    if (contentType === 'link') {

      if (isImageUrl(content)) {
        const newCard = {
          type: contentType,
          link: content,
          tags: [],
        };

        createCard(newCard);
      } else {
        needle('get', content).then((response) => {
          const html = response.body;

          const title = replaceHTMLCharactersWithNormal(parseTag(html, 'title'));
          const description = replaceHTMLCharactersWithNormal(parseMetaTag(html, 'description'));

          const newCard = {
            type: contentType,
            title: title || content,
            text: description,
            link: content,
            tags: [`#${getHostnameFromUrl(content, true)}`],
          };

          createCard(newCard);
        }).catch(() => {
          const newCard = {
            type: contentType,
            title: content,
            link: content,
            tags: [`#${getHostnameFromUrl(content, true)}`],
          };

          createCard(newCard);
        });
      }
    } else if (contentType === 'image') {
      const assetsFolder = `${app.getPath('userData')}/assets`;
      const imagesFolder = `${assetsFolder}/images`;

      const imageAsJPEG = image.toJPEG(70);
      const imagePath = `${imagesFolder}/${uuid()}.jpeg`;

      if (!fs.existsSync(assetsFolder)) {
        fs.mkdirSync(assetsFolder);
      }

      if (!fs.existsSync(imagesFolder)) {
        fs.mkdirSync(imagesFolder);
      }

      fs.writeFile(imagePath, imageAsJPEG, function (error) {
        if (error) return;

        const newCard = {
          type: contentType,
          imagePath: imagePath,
          tags: [],
        };

        createCard(newCard);
      });
    } else {
      const newCard = {
        type: contentType,
        title: '',
        text: content,
        tags: [],
      };

      createCard(newCard);
    }
  });
};


module.exports = { registerGlobalShortcuts };