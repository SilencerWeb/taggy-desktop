const { BrowserWindow } = require('electron');
const isDev = require('electron-is-dev');
const { format } = require('url');
const { resolve } = require('app-root-path');
const Entities = require('html-entities').XmlEntities;

const getPagePath = (pageName) => {
  const devPath = `http://localhost:8000/${pageName}`;

  const prodPath = format({
    pathname: resolve(`renderer/out/${pageName}/index.html`),
    protocol: 'file:',
    slashes: true,
  });

  return isDev ? devPath : prodPath;
};

const isLink = (value) => {
  const urlRegex = /^(https?|ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i;

  return urlRegex.test(value);
};

const isImageUrl = (value) => {
  const imageUrlRegex = /(http)?s?:?(\/\/[^"']*\.(?:png|jpg|jpeg|gif|png|svg))/gi;

  return imageUrlRegex.test(value);
};

const getContentType = (element) => {
  if (typeof element !== 'string') return;

  if (isLink(element)) {
    return 'link';
  } else {
    return 'text';
  }
};

const getWindow = (name) => {
  const allWindows = BrowserWindow.getAllWindows();

  if (name === 'main') {
    return allWindows.find((window) => window.getTitle() === 'Taggy');
  } else if (name === 'success') {
    return allWindows.find((window) => window.getTitle() === 'Saved');
  } else if (name === 'update-progress') {
    return allWindows.find((window) => window.getTitle() === 'Updating Taggy');
  } else if (name === 'preferences') {
    return allWindows.find((window) => window.getTitle() === 'Preferences');
  }

  return null;
};

const getHostnameFromUrl = (url, onlyDomainName) => {
  let hostname;

  if (url.indexOf('//') > -1) {
    hostname = url.split('/')[2];
  } else {
    hostname = url.split('/')[0];
  }

  hostname = hostname.split(':')[0];
  hostname = hostname.split('?')[0];

  if (~hostname.indexOf('www.')) {
    hostname = hostname.slice(4);
  }

  if (onlyDomainName) {
    hostname = hostname.slice(0, hostname.lastIndexOf('.'));
    hostname = hostname.slice(hostname.indexOf('.') + 1);
  }

  return hostname;
};

const parseTag = (string, tag) => {
  const regexp = new RegExp(`<${tag}.*?>(.*)<\\/${tag}>`);

  const match = string.match(regexp);

  if (match) {
    return match[1].trim();
  }
};

const parseMetaTag = (string, tag) => {
  const regexp = new RegExp(`<meta.*?name=".*?${tag}.*?".*?content="([^]*?)".*?>`);

  const match = string.match(regexp);

  if (match) {
    return match[1].trim();
  }
};

const replaceHTMLCharactersWithNormal = (string) => {
  if (!string) return null;

  const entities = new Entities();

  return entities.decode(string);
};

const countTags = (cards) => {
  const tags = {};

  cards.forEach((card) => {
    card.tags && card.tags.forEach((tag) => {
      if (!tags[tag]) {
        tags[tag] = 1;
      } else {
        tags[tag]++;
      }
    });
  });

  const tagsAsArray = Object.keys(tags).map((tag) => {
    return {
      name: tag,
      count: tags[tag],
    };
  });

  const sortedTags = tagsAsArray.sort((tag1, tag2) => tag2.count - tag1.count);

  return sortedTags;
};


module.exports = {
  isImageUrl,
  getPagePath,
  getContentType,
  getWindow,
  getHostnameFromUrl,
  parseTag,
  parseMetaTag,
  replaceHTMLCharactersWithNormal,
  countTags,
};