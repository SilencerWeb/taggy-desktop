const { globalShortcut } = require('electron');

const { store } = require('./database');
const { getWindow } = require('./utils');


const SHORTCUTS_ACCELERATORS = {
  'save-item': process.platform === 'darwin' ? 'Command+Shift+C' : 'Control+Shift+C',
};


const getShortcuts = () => {
  return store.get('shortcuts') || [];
};

const saveShortcuts = (newShortcuts) => {
  store.set('shortcuts', newShortcuts);

  sendShortcutsToMainWindow();
  sendShortcutsToPreferencesWindow();

  return newShortcuts;
};

const getShortcut = (name) => {
  const shortcut = getShortcuts().find((shortcut) => shortcut.name === name);

  if (!shortcut) {
    return saveShortcut(name, SHORTCUTS_ACCELERATORS[name]);
  }

  return shortcut;
};

const saveShortcut = (name, accelerator) => {
  if (globalShortcut.isRegistered(accelerator)) return false;

  try {
    globalShortcut.register(accelerator, () => null);

    globalShortcut.unregisterAll();
  } catch (e) {
    return false;
  }

  const shortcuts = getShortcuts();

  const shortcut = {
    name: name,
    accelerator: accelerator,
  };

  shortcuts.push(shortcut);

  saveShortcuts(shortcuts);

  return shortcut;
};

const editShortcut = (name, accelerator) => {
  if (globalShortcut.isRegistered(accelerator)) return false;

  try {
    globalShortcut.register(accelerator, () => null);

    globalShortcut.unregisterAll();
  } catch (e) {
    return false;
  }

  const shortcuts = getShortcuts();

  const shortcutForEditingIndex = shortcuts.findIndex((shortcut) => shortcut.name === name);

  shortcuts[shortcutForEditingIndex].accelerator = accelerator;

  saveShortcuts(shortcuts);

  return shortcuts[shortcutForEditingIndex];
};

const resetShortcut = (name) => {
  return editShortcut(name, SHORTCUTS_ACCELERATORS[name]);
};

const sendShortcutsToMainWindow = () => {
  const mainWindow = getWindow('main');

  if (!mainWindow) return;

  mainWindow.webContents.send('send-shortcuts', getShortcuts());
};

const sendShortcutsToPreferencesWindow = () => {
  const preferencesWindow = getWindow('preferences');

  if (!preferencesWindow) return;

  preferencesWindow.webContents.send('send-shortcuts', getShortcuts());
};


module.exports = {
  getShortcuts,
  saveShortcuts,

  getShortcut,
  saveShortcut,
  editShortcut,
  resetShortcut,

  sendShortcutsToMainWindow,
  sendShortcutsToPreferencesWindow,
};