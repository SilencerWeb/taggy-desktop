const uuid = require('uuid/v4');

const { store } = require('./database');
const { getWindow } = require('./utils');


const getUser = () => {
  return store.get('user') || null;
};

const createUser = () => {
  store.set('user', { id: uuid() });
};

const normalizeUser = () => {
  const user = getUser();

  store.set('user', { id: user.id });

  sendUserToMainWindow();
};

const sendUserToMainWindow = () => {
  const mainWindow = getWindow('main');

  if (mainWindow) {
    mainWindow.webContents.send('send-user', getUser());
  }
};


module.exports = {
  getUser,
  createUser,
  normalizeUser,

  sendUserToMainWindow,
};