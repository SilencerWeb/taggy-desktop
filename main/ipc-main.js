const { app, ipcMain, shell, clipboard, Menu, nativeImage } = require('electron');
const fs = require('graceful-fs');

const { createApplicationMenu } = require('./menu');
const { changeSetting, sendSettingsToMainWindow, sendSettingsToPreferencesWindow, sendSettingsToUpdateProgressWindow } = require('./settings');
const { sendUserToMainWindow } = require('./user');
const { getCard, editCard, deleteCard, sendTagsToMainWindow, sendCardsToMainWindow } = require('./cards');
const { editShortcut, resetShortcut, sendShortcutsToMainWindow, sendShortcutsToPreferencesWindow } = require('./shortcuts');
const { registerGlobalShortcuts } = require('./global-shortcuts');
const { checkForUpdates } = require('./updater');
const { getWindow } = require('./utils');


const registerIpcMainEvents = () => {

  ipcMain.on('open-link', (event, link) => {
    shell.openExternal(link);
  });

  ipcMain.on('copy', (event, cardId) => {
    const card = getCard(cardId);

    if (card.type === 'link') {
      clipboard.writeText(card.link);
    } else if (card.type === 'image') {
      clipboard.writeImage(nativeImage.createFromPath(card.imagePath));
    } else {
      clipboard.writeText(card.text);
    }
  });

  ipcMain.on('get-user', () => {
    sendUserToMainWindow();
  });

  ipcMain.on('get-tags', () => {
    sendTagsToMainWindow();
  });

  ipcMain.on('get-cards', () => {
    sendCardsToMainWindow();
  });

  ipcMain.on('edit-card', (event, editedCard) => {
    editCard(editedCard);
  });

  ipcMain.on('delete-card', (event, cardId) => {
    deleteCard(cardId);
  });

  ipcMain.on('get-settings', () => {
    sendSettingsToMainWindow();
    sendSettingsToPreferencesWindow();
    sendSettingsToUpdateProgressWindow();
  });

  ipcMain.on('change-settings', (event, newSettings) => {
    if (!newSettings) return;

    const { isLaunchAtLoginEnabled, isAutoUpdatingEnabled, isDarkModeEnabled } = newSettings;

    if (isLaunchAtLoginEnabled !== undefined && isLaunchAtLoginEnabled !== null) {
      changeSetting('isLaunchAtLoginEnabled', isLaunchAtLoginEnabled, true);
    }

    if (isAutoUpdatingEnabled !== undefined && isAutoUpdatingEnabled !== null) {
      changeSetting('isAutoUpdatingEnabled', newSettings.isAutoUpdatingEnabled, true);
    }

    if (isDarkModeEnabled !== undefined && isDarkModeEnabled !== null) {
      changeSetting('isDarkModeEnabled', newSettings.isDarkModeEnabled, true);
    }

    Menu.setApplicationMenu(createApplicationMenu());
  });

  ipcMain.on('check-for-updates', () => checkForUpdates({ enabled: true }));

  ipcMain.on('get-app-version', () => {
    const preferencesWindow = getWindow('preferences');

    if (preferencesWindow) {
      preferencesWindow.webContents.send('send-app-version', app.getVersion());
    }
  });

  ipcMain.on('open-preferences-window', () => {
    const preferencesWindow = getWindow('preferences');

    if (!preferencesWindow) return;

    if (preferencesWindow.isVisible()) {
      preferencesWindow.focus();
    } else {
      preferencesWindow.show();
    }
  });

  ipcMain.on('get-shortcuts', () => {
    sendShortcutsToMainWindow();
    sendShortcutsToPreferencesWindow();
  });

  ipcMain.on('save-shortcut', (event, shortcut) => {
    const isSaved = editShortcut(shortcut.name, shortcut.accelerator);

    if (!isSaved) {
      return false;
    }

    registerGlobalShortcuts();
  });

  ipcMain.on('reset-shortcut', (event, shortcutName) => {
    const isSaved = resetShortcut(shortcutName);

    if (!isSaved) {
      return false;
    }

    registerGlobalShortcuts();
  });
};


module.exports = { registerIpcMainEvents };