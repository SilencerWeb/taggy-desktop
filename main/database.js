const Store = require('electron-store');
const isDev = require('electron-is-dev');


let store;

if (!isDev) {
  store = new Store({
    name: 'taggy-database-production',
    encryptionKey: process.env.DATABASE_ENCRYPTION_KEY,
  });
} else {
  store = new Store({
    name: 'taggy-database-development',
  });
}


module.exports = { store };