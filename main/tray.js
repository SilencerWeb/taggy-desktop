const { app, Tray, nativeImage } = require('electron');
const { resolve } = require('app-root-path');

const { createTrayMenu } = require('./menu');
const { getWindow } = require('./utils');


let tray = null;


const createTray = () => {
  if (tray) return;

  if (process.platform === 'darwin') {
    const trayIcon = nativeImage.createFromPath(resolve('icons/tray-icon-Template.png'));

    tray = new Tray(trayIcon);
  } else {
    tray = new Tray(resolve('icons/icon.ico'));
  }

  tray.on('click', () => {
    const mainWindow = getWindow('main');

    if (!mainWindow) return;

    if (mainWindow.isVisible()) {
      mainWindow.focus();
    } else {
      mainWindow.show();

      if (process.platform === 'darwin') {
        app.dock.show();
      }
    }

    destroyTray();
  });

  tray.on('right-click', () => {
    tray.popUpContextMenu(createTrayMenu());
  });
};

const destroyTray = () => {
  if (tray) {
    tray.destroy();

    tray = null;
  }
};


module.exports = {
  createTray,
  destroyTray,
};