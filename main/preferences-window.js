const { BrowserWindow } = require('electron');

const { getPagePath } = require('./utils');


const createPreferencesWindow = () => {
  const preferencesWindow = new BrowserWindow({
    width: 400,
    height: 250,
    resizable: false,
    minimizable: false,
    maximizable: false,
    title: 'Preferences',
    show: false,
  });

  preferencesWindow.on('blur', () => {
    preferencesWindow.webContents.send('stop-recording-shortcut');
  });

  preferencesWindow.on('close', (event) => {
    event.preventDefault();

    preferencesWindow.hide();
  });

  preferencesWindow.loadURL(getPagePath('preferences'));

  return preferencesWindow;
};


module.exports = { createPreferencesWindow };