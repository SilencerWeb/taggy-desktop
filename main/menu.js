const { app, Menu } = require('electron');
const Store = require('electron-store');

const { checkForUpdates } = require('./updater');
const { getWindow } = require('./utils');


const store = new Store({ name: `taggy-database-production` });


const getPreferencesTemplate = () => ({
  label: 'Preferences',
  accelerator: process.platform === 'darwin' ? 'Cmd+,' : 'F12',
  click: () => {
    const preferencesWindow = getWindow('preferences');

    if (!preferencesWindow) return;

    if (preferencesWindow.isVisible()) {
      preferencesWindow.focus();
    } else {
      preferencesWindow.show();
    }
  },
});

const getApplicationMenuTemplate = () => ([
  {
    label: 'Taggy',
    submenu: [
      {
        label: 'Check for updates',
        click: checkForUpdates,
      },
      { type: 'separator' },
      getPreferencesTemplate(),
      { type: 'separator' },
      {
        label: 'Hide Taggy',
        role: 'hide',
      },
      { role: 'hideothers' },
      { role: 'unhide' },
      { type: 'separator' },
      {
        label: 'Quit Taggy',
        role: 'quit',
      },
    ],
  },
  {
    label: 'Edit',
    submenu: [
      { role: 'undo' },
      { role: 'redo' },
      { type: 'separator' },
      { role: 'cut' },
      { role: 'copy' },
      { role: 'paste' },
      { role: 'selectall' },
    ],
  },
  {
    label: 'View',
    submenu: [
      { role: 'reload' },
      { role: 'forcereload' },
      { role: 'toggledevtools' },
      { type: 'separator' },
      { role: 'togglefullscreen' },
    ],
  },
  {
    role: 'window',
    submenu: [
      { role: 'close' },
      { role: 'minimize' },
      { role: 'zoom' },
    ],
  },
]);

const getTrayMenuTemplate = () => ([
  {
    label: 'Check for updates',
    click: checkForUpdates,
  },
  getPreferencesTemplate(),
  {
    label: 'Quit Taggy',
    click: () => {
      app.quit();
    },
  },
]);


const createApplicationMenu = () => {

  if (process.platform === 'darwin') {
    return Menu.setApplicationMenu(Menu.buildFromTemplate(getApplicationMenuTemplate()));
  }
};

const createTrayMenu = () => {
  return Menu.buildFromTemplate(getTrayMenuTemplate());
};


module.exports = {
  createApplicationMenu,
  createTrayMenu,
};
