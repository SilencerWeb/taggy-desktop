const { app, BrowserWindow } = require('electron');
const electronLocalShortcut = require('electron-localshortcut');

const { createTray } = require('./tray');
const { getPagePath } = require('./utils');


const createMainWindow = (shouldBeVisible) => {
  const mainWindow = new BrowserWindow({
    width: 1200,
    minWidth: 1200,
    height: 700,
    minHeight: 700,
    title: 'Taggy',
    show: false,
  });

  mainWindow.on('ready-to-show', () => {

    if (shouldBeVisible) {
      mainWindow.show();
      mainWindow.focus();
    }
  });


  mainWindow.on('close', (event) => {
    event.preventDefault();

    mainWindow.hide();

    createTray();

    if (process.platform === 'darwin') {
      app.dock.hide();
    }
  });

  mainWindow.loadURL(getPagePath('index'));

  electronLocalShortcut.register(mainWindow, 'ESC', () => {
    mainWindow.webContents.send('close-card-viewer');
  });

  electronLocalShortcut.register(mainWindow, process.platform === 'darwin' ? 'Command+F' : 'Ctrl+F', () => {
    mainWindow.webContents.send('focus-search');
  });

  return mainWindow;
};


module.exports = { createMainWindow };